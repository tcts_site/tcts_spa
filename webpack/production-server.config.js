const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    target: 'node',
    mode: 'production',
    devtool: false,
    context: path.resolve(__dirname, '../'),
    entry: {
        server: ['./app/index.js']
    },
    output: {
        path: path.resolve(__dirname, '../'),
        filename: '[name].min.js',
    },
    resolve: {
        modules: ['node_modules'],
        extensions: ['*', ".js", ".json", ".jsx"]
    },
    externals: [nodeExternals()],
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
            'process.env.SSR': "true",
        }),
    ],
    module: {
        rules: [
            {
                test: [/\.js$/, /\.jsx$/, /\.ejs$/],
                exclude: [/node_modules/],
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    node: {
        __dirname: true,
    }
};