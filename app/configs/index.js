"use strict";

import controllers from '../controllers';
import routes from '../routes/default.json';
import path from 'path';

export default {
    core: {
        controllers: controllers,
    },

    http: {
        listen: 8081,
        domain: 'localhost',
        secret: '',
        cors: true,
        routes: routes,
        static: [path.resolve(__dirname, '../static/')],
        prefix: '/',
        salt: '',
        compress: true,
    },
};
