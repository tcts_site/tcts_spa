export default function isFetching(state = false, action) {
    switch (action.type) {
    case 'FETCHING_TRUE':
        return state = true;
    case 'FETCHING_FALSE':
        return state = false;
    default:
        return state
    }
}