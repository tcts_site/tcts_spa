import React from 'react';
import { connect } from 'react-redux';

import models from '../containers/models';

class RouteContainer extends React.PureComponent {
    syncAPI(model, dispatch, params) {
        const actions = model.actions(params);

        if (actions.length > 0) {
            actions.map(action => dispatch(action));
        }
    }

    componentDidMount() {
        if (this.props.isFetching) {
            const { match: { path } } = this.props;
            const model = models.filter(item => item.path === path).pop();
            const { params } = this.props.match;

            if (model) this.syncAPI(model, this.props.dispatch, params);
        } else {
            this.props.dispatch({type: 'FETCHING_TRUE'});
        }
    }

    render() {
        const { Component } = this.props;
        return <Component {...this.props} />;
    }
}

export default connect(
    state => ({ isFetching: state.isFetching })
)(RouteContainer);

