import React from 'react';
import { renderRoutes } from "react-router-config";
import Routes from './routes';


export default class App extends React.Component {
    render() {
        return renderRoutes(Routes);
    }
}
