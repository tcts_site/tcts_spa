import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import PriceReducer from './price.reducer';

export default combineReducers({
    blur: false,
    isFetching: false,
    PriceReducer,
    routing: routerReducer,
});