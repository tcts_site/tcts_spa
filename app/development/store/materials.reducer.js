import * as CONST from './../containers/materials/constants';

const InitialState = {
    data: {}
};

export default (state = InitialState, action) => {
    if (action.type && CONST[action.type]) {
        return {
            ...state,
            state: action.type,
            data: action.data || []
        };
    }
    return state;
};