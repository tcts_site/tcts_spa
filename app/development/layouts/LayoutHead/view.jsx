import React from 'react';
import Helmet from 'react-helmet';

export default class LayoutHead extends React.Component {
    render() {
        return (
            <Helmet>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="Description" content="Актуальный новости на русском" />
                <link rel="icon" href="/favicon.ico" type="image/x-icon" />

                {this.props.children}
            </Helmet>
        );
    }
}
