import React from 'react';
import { matchRoutes } from 'react-router-config';
import { Route } from 'react-router-dom';

import RouteWrapper from '../../helpers/RouteWrapper';
import Header from '../../components/Header';
import CalcModal from "../../components/CalcModal";
import Feedback from "../../components/Feedback";

// Через LayoutContext можно передавать глобальные props
export const LayoutContext = React.createContext();

class LayoutMain extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            fixed: false,
            transform: false,
            inBlur: false,
            isFeedback: false,
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handlerScroll.bind(this), false);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handlerScroll.bind(this), false);
    }

    handlerScroll() {
        if (!this.state.inBlur) {
            let scrolled = window.pageYOffset || document.documentElement.scrollTop;
            this.setState({ transform: scrolled > 600 });
            this.setState({ fixed: scrolled > 400 });
        }
    }

    inBlur() {
        setTimeout(() => {
            this.setState({
                inBlur: !this.state.inBlur,
            });
        }, 100);
    }

    toogleFeedback() {
        this.setState({
            isFeedback: !this.state.isFeedback,
        });
    }

    renderRoutes = () => {
        const { route, location } = this.props;

        return matchRoutes(route.routes, location.pathname).map(({route}) => (
            <LayoutContext.Provider value={{inBlur: this.inBlur.bind(this), toogleFeedback: this.toogleFeedback.bind(this)}}>
                <Route
                    key={route.path}
                    exact={route.exact}
                    path={route.path}
                    render={() => <RouteWrapper Component={route.component} />}
                />
            </LayoutContext.Provider>
        ));
    };

    render() {
        const { fixed, inBlur, transform } = this.state;

        return (
            <React.Fragment>
                <div className={`App-root ${inBlur ? 'in-blur' : ''}`}>
                    <div className={`App-header ${transform ? 'transform' : ''} ${fixed ? 'fixed' : ''}`}>
                        <Header/>
                    </div>
                    {this.renderRoutes()}
                </div>
                {this.state.inBlur && <CalcModal inBlur={this.inBlur.bind(this)} isOpen={this.state.inBlur} />}
                {this.state.isFeedback && <Feedback toogleFeedback={this.toogleFeedback.bind(this)} isOpen={this.state.isFeedback} />}
            </React.Fragment>
        );
    }
}

export default LayoutMain;
