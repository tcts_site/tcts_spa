import React from 'react';

import TopHeaderBlock from '../components/TopHeaderBlock';
import TopBGBlock from "../components/TopBGBlock";
import Contacts from "../components/Contacts";
import Footer from "../components/Footer";

export default class IndexController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <div className={`App-content contacts-page`}>
                    <TopBGBlock
                        image="https://cargolink.ru/ls/uploads/topics/preview/00/00/77/61/18aeb90a74.jpg"
                    />
                    <div className="Section-root">
                        <TopHeaderBlock
                            title="Напишите"
                            subtitle="Есть вопросы?"
                        />
                        <Contacts />
                    </div>
                </div>
                <div className="App-footer">
                    <Footer/>
                </div>
            </React.Fragment>
        );
    }
}
