import React from 'react';

import TopBGBlock from '../components/TopBGBlock';
import TopHeaderBlock from '../components/TopHeaderBlock';
import CargoTaxi from "../components/CargoTaxi";
import CargoRussia from "../components/CargoRussia";
import Customers from "../components/Customers";
import Legal from "../components/Legal";
import Footer from "../components/Footer";
import InfoBlock from "../components/InfoBlock";

export default class IndexController extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <div className="App-content uslugi-page">
                    <TopBGBlock
                        image="https://alfabank.ru/f/3/sme/agent/regist/banner.jpg"
                    />
                    <div className="Section-root">
                        <TopHeaderBlock
                            title="Перевозим всё!"
                            subtitle="Наши услуги"
                            summary="Мы выполняем все виды грузоперевозок по Москве и Московской области, а также по всей России по самым выгодным тарифам"
                        />
                        <CargoTaxi />
                        <CargoRussia />
                        <Customers />
                        <Legal />
                        <InfoBlock />
                    </div>
                </div>
                <div className="App-footer">
                    <Footer/>
                </div>
            </React.Fragment>
        );
    }
}
