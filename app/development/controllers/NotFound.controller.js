import React, {PureComponent} from 'react';
import LayoutHead from '../layouts/LayoutHead';
import { Route } from 'react-router-dom';

const Status = ({ code, children }) => (
    <Route render={({ staticContext }) => {
        if (staticContext)
            staticContext.status = code
        return children
    }}/>
);

export default class NotFoundController extends PureComponent {
    render() {
        return (
            <React.Fragment>
                <LayoutHead>
                    <title>Такой страницы не существует</title>
                </LayoutHead>
                <Status code={404}>
                    <div className="notfound">
                        <h1>Упс! Ты не туда попал!</h1>
                    </div> 
                </Status>
            </React.Fragment>
        );
    }
} 
