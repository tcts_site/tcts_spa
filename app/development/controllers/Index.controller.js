import React from 'react';

import TopHeaderBlock from "../components/TopHeaderBlock";
import TopBGBlock from "../components/TopBGBlock";
import { LayoutContext } from "../layouts/LayoutMain/view";


export default class IndexController extends React.Component {
    static contextType = LayoutContext;

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <div className={`App-content index-page`}>
                    <TopBGBlock
                        image="https://cargolink.ru/ls/uploads/topics/preview/00/00/77/61/18aeb90a74.jpg"
                    />
                    <div className="Section-root">
                        <TopHeaderBlock
                            title="Перевозим грузы по Москве и области"
                            subtitle="Грузоперевозки - это просто!"
                            summary="для частных клиентов, юридических лиц, интернет-магазинов и сетевым клиентам"
                            btnTitle="Рассчитать цену"
                            linkBtn={'Узнать больше'}
                            inBlur={this.context.inBlur}
                        />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
