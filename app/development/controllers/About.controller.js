import React from 'react';

import TopBGBlock from '../components/TopBGBlock';
import TopHeaderBlock from '../components/TopHeaderBlock';
import ExcellenceBlock from '../components/ExcellenceBlock';
import ServicesBlock from '../components/ServicesBlock';
import TustBlock from '../components/TustBlock';
import InfoBlock from '../components/InfoBlock';
import Footer from "../components/Footer";

export default class AboutController extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <div className="App-content about-page">
                    <TopBGBlock
                        image="https://alfabank.ru/f/3/everyday/debit-cards/cash-back-card/cash2.jpg"
                    />
                    <div className="Section-root">
                        <TopHeaderBlock
                            title="Компания Транстехсервис"
                            summary="Мы один из ведущих компаний на рынке транспортных и логистических услуг Москвы и области, а также один из крупнейших грузоперевозчиков по всей России."
                            subtitle="О нас"
                        />
                        <ExcellenceBlock />
                        <ServicesBlock />
                        <TustBlock />
                        <InfoBlock />
                    </div>
                </div>

                <div className="App-footer">
                    <Footer/>
                </div>
            </React.Fragment>
        );
    }
}
