import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import createSagaMiddleware, {END} from "redux-saga";
import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import './scss/index.scss';

import Application from './app';
import ApplicationSaga from './sagas';
import ApplicationReducer from "./store";
import ApiMiddleware from "./middlewares/api";

const preloadedState = window.__PRELOADED_STATE__;
delete window.__PRELOADED_STATE__;

const history = createHistory();
const routersMiddleware = routerMiddleware(history);
const SagaMiddleware = createSagaMiddleware();

const store = createStore(
    ApplicationReducer,
    preloadedState,
    composeWithDevTools(applyMiddleware(
        SagaMiddleware,
        routersMiddleware,
        ApiMiddleware,
    )),
);

store.runSaga = SagaMiddleware.run;
store.close = () => store.dispatch(END);
store.runSaga(ApplicationSaga);

const ReactRender = Component => {
    return ReactDOM.hydrate(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Component />
            </ConnectedRouter>
        </Provider>, document.getElementById('root')
    );
};

ReactRender(Application);

if (module.hot) {
    module.hot.accept('./app', () => {
        const NextApp = require('./app').default;
        ReactRender(NextApp);
    });
}