import { put, call, takeLatest } from 'redux-saga/effects';
import * as CONST from './constants';
import * as API from './api';

// PRICE_GET
function* PriceGet() {
    yield put({
        type: CONST.START_PRICE_GET,
    });

    try {
        const res = yield call(API.PriceGet);
        yield put({
            type: CONST.FINISH_PRICE_GET,
            data: res,
        });
    }
    catch (error) {
        yield put({
            type: CONST.ERROR_PRICE_GET,
            data: error
        });
    }
}

// PRICE_UPDATE
function* PriceUpdate({ payload }) {
    yield put({
        type: CONST.START_PRICE_UPDATE,
        refreshing: true,
    });

    try {
        const res = yield call(API.PriceUpade, payload);
        yield put({
            type: CONST.FINISH_PRICE_UPDATE,
            data: res,
        });
    }
    catch (error) {
        yield put({
            type: CONST.ERROR_PRICE_UPDATE,
            data: error
        });
    }
}

export default function*() {
    yield takeLatest('PRICE_GET', PriceGet);
    yield takeLatest('PRICE_UPDATE', PriceUpdate);
}