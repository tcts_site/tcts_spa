import Fetcher from './../../helpers/fetcher';

export function PriceUpade(data) {
    return Fetcher('price', 'PUT', data);
}

export function PriceGet() {
    return Fetcher("price", "GET")
}
