import React, { PureComponent } from 'react';

class TustBlock extends PureComponent {
    render() {
        const defaultClass = 'tust-block';

        return (
            <div className={`${defaultClass}`}>
                <div className={`content-block`}>
                    <div className={`content-block__inner`}>
                        <h2 className={`content-block__title content-block__title_center`}>Нам доверяют</h2>
                        <ul className={`card-list card-list_top`}>
                            <li className={`card-list__item card-list__item_1to5 card-list__item_type-4`}>
                                <span className={`dn`}>Adidas</span>
                                <div className={`${defaultClass}__icon ${defaultClass}__iconAdidas`} />
                            </li>
                            <li className={`card-list__item card-list__item_1to5 card-list__item_type-4`}>
                                <span className={`dn`}>HandM</span>
                                <div className={`${defaultClass}__icon ${defaultClass}__iconHandM`} />
                            </li>
                            <li className={`card-list__item card-list__item_1to5 card-list__item_type-4`}>
                                <span className={`dn`}>Nike</span>
                                <div className={`${defaultClass}__icon ${defaultClass}__iconNike`} />
                            </li>
                            <li className={`card-list__item card-list__item_1to5 card-list__item_type-4`}>
                                <span className={`dn`}>Puma</span>
                                <div className={`${defaultClass}__icon ${defaultClass}__iconPuma`} />
                            </li>
                            <li className={`card-list__item card-list__item_1to5 card-list__item_type-4`}>
                                <span className={`dn`}>PullandBear</span>
                                <div className={`${defaultClass}__icon ${defaultClass}__iconPullandBear`} />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default TustBlock;
