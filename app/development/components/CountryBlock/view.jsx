import React from 'react';

export default class CountryBlock extends React.PureComponent {
    render() {
        const defaultClass = 'country-block';

        return (
            <div className={`${defaultClass} mt-30`}>
                <div className={`${defaultClass}__tiles`}>
                    <div className={`${defaultClass}__tile`}>
                        <h4 className={`headline headline_type-3`}>Оставьте заявку! Мы перезвоним!</h4>
                    </div>
                </div>
            </div>
        );
    }
}
