import React, { PureComponent } from 'react';

class ExcellenceBlock extends PureComponent {
    render() {
        const defaultClass = 'excellence-block';

        return (
            <div className={`${defaultClass}`}>
                <div className={`content-block`}>
                    <div className={`content-block__inner`}>
                        <h2 className={`content-block__title content-block__title_center`}>Почему выбирают нас</h2>
                        <ul className={`card-list card-list_top`}>
                            <li className={`card-list__item card-list__item_1to3 card-list__item_type-3`}>
                                <div className={`${defaultClass}__icon ${defaultClass}__iconPercent`} />
                                <div className={`paragraph paragraph_type-4`}>100% довольных<br/>клиентов</div>
                            </li>
                            <li className={`card-list__item card-list__item_1to3 card-list__item_type-3`}>
                                <div className={`${defaultClass}__icon ${defaultClass}__iconDesctop`} />
                                <div className={`paragraph paragraph_type-4`}>Современные<br/>транспортные средства</div>
                            </li>
                            <li className={`card-list__item card-list__item_1to3 card-list__item_type-3`}>
                                <div className={`${defaultClass}__icon ${defaultClass}__iconShield`} />
                                <div className={`paragraph paragraph_type-4`}>Качественная услуга<br/>и доступная цена</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default ExcellenceBlock;
