import React, { PureComponent } from 'react';
import { NavHashLink as NavLink } from 'react-router-hash-link';

class ServicesBlock extends PureComponent {
    render() {
        const defaultClass = 'services-block';

        const scrollWithOffset = (el, offset) => {
            const elementPosition = el.offsetTop - offset;
            window.scroll({
                top: elementPosition,
                left: 0,
                behavior: "smooth"
            });
        };

        return (
            <div className={`${defaultClass}`}>
                <div className={`content-block`}>
                    <div className={`content-block__inner`}>
                        <h2 className={`content-block__title content-block__title_center`}>Наши услуги</h2>
                        <ul className={`card-list card-list_top card-list_wrap`}>
                            <li className={`card-list__item card-list__item_1to2 card-list__item_type-1`}>
                                <h4 className={`headline headline_type-2`}>Грузовое такси</h4>
                                <p className={`paragraph paragraph_type-2 mb-30`}>
                                    Грузовое такси – это услуга, при помощи которой вы можете быстро и в то же время дешево, срочно перевести малогабаритный груз в пределах Москвы и Московской области.
                                </p>
                                <NavLink to="/services#taxi" scroll={el => scrollWithOffset(el, 100)} className={`${defaultClass}__button`}>Узнать больше</NavLink>
                            </li>
                            <li className={`card-list__item card-list__item_1to2 card-list__item_type-1`}>
                                <h4 className={`headline headline_type-2`}>Грузоперевозки по всей России</h4>
                                <p className={`paragraph paragraph_type-2 mb-30`}>
                                    Грузоперевозки по России – это качественная и профессиональная услуга по транспортировке любого вида груза по России, которая осуществляется высококвалифицированными специалистами компании «Транстехсервис» в сжатые сроки, согласно заказу клиента.
                                </p>
                                <NavLink to="/services#russia" scroll={el => scrollWithOffset(el, 100)} className={`${defaultClass}__button`}>Узнать больше</NavLink>
                            </li>
                            <li className={`card-list__item card-list__item_1to2 card-list__item_type-1`}>
                                <h4 className={`headline headline_type-2`}>Доставка сетевым клиентам</h4>
                                <p className={`paragraph paragraph_type-2 mb-30`}>
                                    Мы работаем с крупными компаниями и с предприятиями среднего и малого бизнеса. Доставка в торговые сети осуществляется стандартными евро фурами и грузовыми автомобилями меньшей вместимости.
                                </p>
                                <NavLink to="/services#clients" scroll={el => scrollWithOffset(el, 100)} className={`${defaultClass}__button`}>Узнать больше</NavLink>
                            </li>
                            <li className={`card-list__item card-list__item_1to2 card-list__item_type-1`}>
                                <h4 className={`headline headline_type-2`}>Юридическим лицам</h4>
                                <p className={`paragraph paragraph_type-2 mb-30`}>
                                    Грузоперевозки для юридических лиц осуществляются по договору, в котором прописываются все условия и правила, не поодлежащие изменению после его подписания.
                                </p>
                                <NavLink to="/services#legal" scroll={el => scrollWithOffset(el, 100)} className={`${defaultClass}__button`}>Узнать больше</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default ServicesBlock;
