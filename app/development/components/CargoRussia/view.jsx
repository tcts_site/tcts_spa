import React, { PureComponent } from 'react';
import CountryBlock from "../CountryBlock";

class CargoTaxi extends PureComponent {
    render() {
        const defaultClass = 'cargo-russia';

        return (
            <div className={`${defaultClass}`} id="russia">
                <div className={`content-block`}>
                    <div className={`content-block__inner`}>
                        <h2 className={`content-block__title`}>Грузоперевозки по России</h2>
                        <div className={`content-block__text`}>
                            <h3 className={`headline headline_type-1`}>Что это?</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                Грузоперевозки по России – это качественная и профессиональная услуга по транспортировке любого вида груза по России, которая осуществляется высококвалифицированными специалистами компании «Транстехсервис» в сжатые сроки, согласно заказу клиента.
                            </p>
                            <h3 className={`headline headline_type-1`}>Что мы предлагаем?</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                В какую бы часть Российской Федерации не предназначалась доставка груза, мы выполним ее без малейших проблем! Вся организация процесса грузоперевозки: начиная от подбора подходящего автотранспорта, погрузки-выгрузки, отслеживание груза, а так же охраны груза, проводится специалистами компании. Вам следует лишь довериться высокому уровню профессионализма наших сотрудников.
                            </p>
                            <CountryBlock />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CargoTaxi;
