import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Total from '../calculator/total';
import Addresses from '../calculator/addresses';
import Car from '../calculator/car';
import ExtraOptions from '../calculator/extra-options';
import FormResult from '../calculator/form-result';
import * as Actions from '../../containers/price/actions';
import * as CONST from "../../containers/price/constants";

const mapStateToProps = state => ({
    answer: state.PriceReducer
});

const mapDispatchProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchProps
)(class CalcModal extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            inputToAddress: false,
            inputFromAddress: false,
            text: null,
            toggleTooltip: {
                toAddress: false,
                fromAddress: false,
            },
            fadeIn: null,
            tabs: 1,
            priceData: [],
            car: null,
            addrs: null,
            extra: null,
            time: null,
            pallet: null,
            curtains: null,
            nds: false,
            distance: null,
        };
    }

    componentDidMount() {
        this.props.actions.price('PRICE_GET');

        this.setState({
            fadeIn: 'fadeInStart',
            fadeOut: null,
        });
        setTimeout(() => {
            this.setState({
                fadeIn: 'fadeInEnd',
            });
        }, 100);
    }

    componentWillReceiveProps(props) {
        if (props.answer && props.answer.state) {
            switch (props.answer.state) {
                case CONST.FINISH_PRICE_GET:
                    this.setState({
                        priceData: props.answer.data.result,
                    });
                    break;

                case CONST.ERROR_PRICE_GET:
                    this.setState({
                        priceData: [],
                    });
                    break;

                default: break;
            }
        }
    }

    createdRef = (toAddr, fromAddr) => {
        this.setState({
            inputToAddress: toAddr,
            inputFromAddress: fromAddr,
        })
    };

    closeModal() {
        this.props.inBlur();
    }

    checkInputs = () => {
        const { inputToAddress, inputFromAddress } = this.state;
        const inputs = [
            inputToAddress,
            inputFromAddress
        ];

        const hasValue = input => input.current.value.length === 0;
        const successData = inputs.filter(hasValue);
        inputs.forEach(input => input.current.classList.remove('input_invalid'));

        if (successData.length === 0) {
            return true;
        } else {
            successData.forEach(input => input.current.classList.add('input_invalid'));
            return false;
        }
    };

    handlerNext = value => {
        if (value === 2 && !this.checkInputs()) {
            return;
        }

        if (value === 3 && this.state.car === null) {
            alert('Выберите машину');
            return;
        }

        this.setState({
            tabs: value,
        });
    };

    checkCars = (value) => {
        let result = [];
        let result1 = [];
        for (let i = 1; i <= value.Pallet; i++) {
            result.push({
                value: i,
                text: i,
                types: 'pallet'
            })
        }

        for (let i = value.Time; i <= 12; i++) {
            result1.push({
                value: i,
                text: i,
                types: 'time'
            })
        }

        let result2 = [
            { value: 0, text: 'Нет', types: 'curtains' },
            { value: 1000, text: '5-10 тн боковая', types: 'curtains' },
            { value: 2000, text: '5-10 тн верхняя', types: 'curtains' },
            { value: 2000, text: '20 тн боковая', types: 'curtains' },
            { value: 4000, text: '20 тн верхняя', types: 'curtains' },
        ];

        this.setState({
            car: value,
            extra: {
                pallet: result,
                time: result1,
                curtains: result2,

            },
            time: result1[0],
            pallet: result[0],
            curtains: result2[0],
        });
    };

    checkForms = value => {
        if (typeof value === 'boolean') {
            this.setState({
                nds: value,
            });
        } else {
            this.setState({
                [value.types]: value,
            });
        }
    };

    mapDistance = (value, all) => {
        this.setState({
           distance: {value, all},
        });
    };

    render() {
        const defaultClass = 'CalcModal';
        const { isOpen } = this.props;
        const { tabs, priceData } = this.state;

        return (
            <div className={`${defaultClass} ${isOpen ? 'CalcModal_open' : ''} ${this.state.fadeIn}`}>
                <div className={`${defaultClass}__wrapper mt-50`}>
                    {/*<div className="breadcrumb-container">*/}
                        {/*<BreadCrumbs tabs={tabs} handlerNext={this.handlerNext} />*/}
                    {/*</div>*/}
                    <div className="container-row">
                        <div className={`${defaultClass}__form`}>
                            <Addresses
                                tabs={tabs}
                                handlerNext={this.handlerNext}
                                createdRef={this.createdRef}
                            />

                            <Car
                                tabs={tabs}
                                handlerNext={this.handlerNext}
                                priceData={priceData}
                                checkCars={this.checkCars}
                            />

                            <ExtraOptions
                                car={this.state.car}
                                extra={this.state.extra}
                                time={this.state.time}
                                pallet={this.state.pallet}
                                curtains={this.state.curtains}
                                tabs={tabs}
                                handlerNext={this.handlerNext}
                                checkForms={this.checkForms}
                            />

                            <FormResult
                                tabs={tabs}
                                time={this.state.time}
                                car={this.state.car}
                                curtains={this.state.curtains}
                                distance={this.state.distance}
                                nds={this.state.nds}
                                handlerNext={this.handlerNext}
                            />
                        </div>
                        <div className={`${defaultClass}__info`}>
                            <Total
                                tabs={tabs}
                                inputToAddress={this.state.inputToAddress}
                                inputFromAddress={this.state.inputFromAddress}
                                car={this.state.car}
                                time={this.state.time}
                                pallet={this.state.pallet}
                                curtains={this.state.curtains}
                                nds={this.state.nds}
                                updateData={this.updateData}
                                mapDistance={this.mapDistance}
                            />
                        </div>
                    </div>
                </div>
                <div className={`${defaultClass}__close`} onClick={this.closeModal.bind(this)}/>
            </div>
        );
    }
});
