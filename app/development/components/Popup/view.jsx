import React, { PureComponent } from 'react';
import classNames from 'classnames';

class Popup extends PureComponent {
    constructor(props) {
        super(props);
        this.inputYourName = React.createRef();
        this.inputYourPhone = React.createRef();
        this.buttonSend = React.createRef();
        this.state = {
            fadeIn: null,
        }
    }

    componentDidMount() {
        this.setState({
            fadeIn: 'fadeInStart',
            fadeOut: null,
        });
        setTimeout(() => {
            this.setState({
                fadeIn: 'fadeInEnd',
            });
        }, 100);
    }

    closeModal = () => {
        this.setState({
            fadeIn: 'fadeOutStart',
        });
        setTimeout(() => {
            this.setState({
                fadeIn: 'fadeOutEnd',
            });
            this.props.toogleFeedback();
        }, 100);
    };

    sendMail = () => {
        const _this = this;
        fetch("/api/feedback/send", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: 'Test',
                phone: 79659211884,
                type: 'feedback',
            })
        })
            .then(res => res.json())
            .then(res => {
                _this.closeModal();
            });
    };

    render() {
        const defaultClass = 'Popup';
        const inputYourName = {
            key: 'inputYourName',
            className: `${defaultClass}__input ${defaultClass}__input_your-name`,
            id: 'your-name',
            type: 'text',
            placeholder: 'Как Вас зовут',
            ref: this.inputYourName,
            name: 'your-name',
        };

        const inputYourPhone = {
            key: 'inputYourPhone',
            className: `${defaultClass}__input ${defaultClass}__input_your-phone`,
            id: 'your-phone',
            type: 'text',
            placeholder: 'Номер Вашего телефон',
            ref: this.inputYourPhone,
            name: 'your-phone',
        };

        const buttonSend = {
            key: 'buttonSend',
            className: `${defaultClass}__button ${defaultClass}__button_send`,
            id: 'button_send',
            ref: this.buttonSend,
            name: 'button_send',
        };

        const classProps = classNames(
            defaultClass,
            this.state.fadeIn && this.state.fadeIn,
        );

        return (
            <div className={classProps}>
                <div className={`${defaultClass}__overlay`} onClick={this.closeModal.bind(this)} />
                <div className={`${defaultClass}__block`}>
                    <p className={`${defaultClass}__header`}>Обратный звонок</p>
                    <input {...inputYourName} />
                    <input {...inputYourPhone} />
                    <button {...buttonSend} onClick={this.sendMail}>Отправить</button>
                    <div className={`${defaultClass}__close`} onClick={this.closeModal} />
                </div>
            </div>
        );
    }
}

export default Popup;
