import React, { PureComponent } from 'react';

class PriceBlock extends PureComponent {
    render() {
        const defaultClass = 'PriceBlock';
        return (
            <div className={`${defaultClass}`}>
                <div className={`${defaultClass}__content`}>
                    <div className={`${defaultClass}__inner`}>
                        <table className={`${defaultClass}__table`}>
                            <caption className={`${defaultClass}__title`}>Наши цены</caption>
                            <tbody>
                                <tr className={`${defaultClass}__table-row bg-color--grey`}>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_header`}>Тоннаж</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_header`}>Параметры А/М</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_header`}>Цена за 1 час работы</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_header`}>Стоимость за 1 км за МКАД (руб.км)</span>
                                    </td>
                                </tr>
                                <tr className={`${defaultClass}__table-row`}>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content`}>1 Т</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red text-small`}>
                                            Длина до 2 м
                                            <br/>
                                            Объем до 5 м<sup>3</sup>
                                        </span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>500 ₽</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>16 ₽</span>
                                    </td>
                                </tr>
                                <tr className={`${defaultClass}__table-row bg-color--grey`}>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content`}>1.5 Т</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red text-small`}>
                                            Длина до 2 м
                                            <br/>
                                            Объем до 5 м<sup>3</sup>
                                        </span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>650 ₽</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>17 ₽</span>
                                    </td>
                                </tr>
                                <tr className={`${defaultClass}__table-row`}>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content`}>3 Т</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red text-small`}>
                                            Длина до 2 м
                                            <br/>
                                            Объем до 5 м<sup>3</sup>
                                        </span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>700 ₽</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <p className={`${defaultClass}__table_content color-text-red`}>17 ₽</p>
                                    </td>
                                </tr>
                                <tr className={`${defaultClass}__table-row bg-color--grey`}>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content`}>5 Т</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red text-small`}>
                                            Длина до 2 м
                                            <br/>
                                            Объем до 5 м<sup>3</sup>
                                        </span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>900 ₽</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>17 ₽</span>
                                    </td>
                                </tr>
                                <tr className={`${defaultClass}__table-row`}>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content`}>10 Т</span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red text-small`}>
                                            Длина до 2 м
                                            <br/>
                                            Объем до 5 м<sup>3</sup>
                                        </span>
                                    </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>1000 ₽</span>
                                     </td>
                                    <td className={`${defaultClass}__table-col`}>
                                        <span className={`${defaultClass}__table_content color-text-red`}>21 ₽</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p className={`${defaultClass}__ps`}>
                            * При работе за пределами МКАД от 0-50 км стоимость работ рассчитывается по комбинированной схеме: общее время + километраж
                        </p>
                        <h4 className={`${defaultClass}__advantages`}>
                            Если рассматривать грузовое такси в Москве с другим видом доставки груза, оно имеет массу преимуществ, среди которых можно выделить:
                        </h4>
                        <ul className={`${defaultClass}__list`}>
                            <li>
                                <h5>1. Оперативность</h5>
                                <p>Заказ грузотакси осуществляется непосредственно в момент, когда необходимо выполнить ранспортировку, то есть это идеальный вариант, если вам необходимо доставить вещи в кратчайшие сроки и нет возможности заказать услугу заранее</p>
                            </li>
                            <li>
                                <h5>2. Выгодная цена</h5>
                                <p>Для своих клиентов мы установили выгодные тарифы, которые позволяют экономить деньги, без ущерба качеству. Грузовое такси стоит дешево: цена на услуги начинаются от 400 руб. за 1 час, что совсем недорого для крупного мегаполиса</p>
                            </li>
                            <li>
                                <h5>3. Надежность</h5>
                                <p>В своей работе мы уделяем особое внимание сохранности груза во время транспортировки, поэтому вы можете не беспокоиться о том, что в процессе переезда вещи повредятся или потеряются. Мы гарантируем, что все будет доставлено в целости и сохранности</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default PriceBlock;
