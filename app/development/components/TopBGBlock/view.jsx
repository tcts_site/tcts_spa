import React, { PureComponent } from 'react';

class BackgroundHeader extends PureComponent {
    render() {
        const defaultClass = 'TopBGBlock';
        const style = {
          backgroundImage: `url(${this.props.image})`,
        };

        return (
            <div className={`${defaultClass}`} style={style}>
                <div className={`${defaultClass}__overlay`} />
            </div>
        );
    }
}

export default BackgroundHeader;
