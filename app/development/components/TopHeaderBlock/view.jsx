import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

class TopHeaderBlock extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {}
    }

    handleClick() {
        this.props.inBlur();
    }

    render() {
        const defaultClass = 'TopHeaderBlock';

        return (
            <div className={`${defaultClass}`}>
                <div className={`${defaultClass}__content`}>
                    <div className={`${defaultClass}__header`}>
                        {this.props.subtitle && (
                            <p className={`${defaultClass}__subtitle`}>
                                {this.props.subtitle}
                            </p>
                        )}
                        {this.props.title && (
                            <h1 className={`${defaultClass}__title`}>
                                {this.props.title}
                            </h1>
                        )}
                        {this.props.summary && (
                            <p className={`${defaultClass}__summary`}>
                                {this.props.summary}
                            </p>
                        )}
                        <div className={`${defaultClass}__top-btn`}>
                            {this.props.btnTitle && (
                                <div className={`${defaultClass}__button`} onClick={this.handleClick.bind(this)}>
                                    {this.props.btnTitle}
                                </div>
                            )}
                            {this.props.linkBtn && (
                                <Link to='/about' className={`${defaultClass}__button ${defaultClass}__link`}>
                                    {this.props.linkBtn}
                                </Link>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TopHeaderBlock;
