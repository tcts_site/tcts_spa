import React, { PureComponent } from 'react';

import { LayoutContext } from "../../layouts/LayoutMain/view";

class InfoBlock extends PureComponent {
    static contextType = LayoutContext;

    handleClick() {
        this.context.toogleFeedback();
    }

    render() {
        const defaultClass = 'info-block';

        return (
            <div className={`${defaultClass}`}>
                <div className={`content-block content-block_red`}>
                    <div className={`content-block__inner`}>
                        <div className={`${defaultClass}__warning-text`}>
                            Получите консультацию по телефону:
                        </div>
                        <div className={`${defaultClass}__warning-phone`}>
                            <a href="tel:84952011591">+7 (495) 201-15-91</a>
                        </div>
                        <div className={`${defaultClass}__warning-btn`}>
                            <div className="ili">или</div>
                            <button onClick={this.handleClick.bind(this)}>Оставьте заявку</button>
                            <div className="text">Мы свяжемся с Вами в удобное для Вас время</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default InfoBlock;
