import React, { PureComponent } from 'react';
import CK from './coordinatesCK';
import TTK from './coordinatesTTK';
import MKAD from './coordinatesMKAD';
import coordsTTK from './coordsTTK';
import coordsCITY from './coordsCITY';

class YaMaps extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            inputTo: false,
            inputFrom: false,
            coords: {
                bounds: [[34.40350341796876, 54.767450087725074], [41.215026855468764, 56.62841404541838]],
                center: [55.751574, 37.573856],
                city: coordsCITY,
                ttk: coordsTTK,
                ck: [],
            }
        };
        this.map = false;
        this.cityPoly = false;
        this.ttkPoly = false;
        this.mark = false;
        this.route=false;
        this.ttk = false;
        this.ck = false;
        this.multiRoute = false;
    }

    componentDidMount() {
        window.ymaps.ready(this.init, this);
    }

    // Инициализация Яндекс Карты
    init() {
        const { coords } = this.state;
        const { inputToAddress, inputFromAddress } = this.props;

        this.setState({
            inputTo: inputToAddress,
            inputFrom: inputFromAddress,
        });

        this.map = new window.ymaps.Map('map', {
            center: coords.center,
            zoom: 8,
            controls: ['zoomControl',  'fullscreenControl', 'trafficControl'],
            behaviors: ['drag', 'dblClickZoom', 'multiTouch', 'rightMouseButtonMagnifier']
        });

        this.cityPoly = new window.ymaps.Polygon(coords.city);
        this.ttkPoly = new window.ymaps.Polygon(coords.ttk);
        window.ymaps.geoQuery(this.cityPoly).setOptions('visible', false).addToMap(this.map);
        window.ymaps.geoQuery(this.ttkPoly).setOptions('visible', false).addToMap(this.map);

        this.addSuggest();
    }

    // Выпадающий список подходящих адресов
    addSuggest() {
        const { coords, inputTo, inputFrom } = this.state;

        this.toAddr = new window.ymaps.SuggestView(inputTo.current, {
            results: 5,
            // boundedBy: coords.bounds
        });

        this.fromAddr = new window.ymaps.SuggestView(inputFrom.current, {
            results: 5,
            boundedBy: coords.bounds
        });

        this.toAddr.events.add('select', this.search, this);
        this.fromAddr.events.add('select', this.search, this);
    }

    // Поиск
    search() {
        const self = this;
        const { inputTo, inputFrom } = this.state;

        if (this.multiRoute) {
            this.map.geoObjects.remove(this.multiRoute);
            this.multiRoute = false;
        }

        let addrs = [];
        !!inputTo.current.value && addrs.push(inputTo.current.value);
        !!inputFrom.current.value && addrs.push(inputFrom.current.value);

        if (addrs.length === 0) return;

        this.multiRoute = new window.ymaps.multiRouter.MultiRoute({
            referencePoints: [
                addrs[0],
                addrs[1],
            ],
            params: {
                reverseGeocoding: true,
                results: 1,
            }
        }, {
            boundsAutoApply: false,
            wayPointDraggable: true,
            preventDragUpdate: true,
        });

        this.multiRoute.model.events.add('requestsuccess', function() {
            self.multiRoute.getRoutes().each(function (route) {
                let distance = self.multiRoute.getRoutes().get(0).properties.getAll();
                var edges = [];

                route.getPaths().each(function (path) {
                    path.getSegments().each(function (segment) {
                        const coordinates = segment.geometry.getCoordinates();
                        for (let i = 1, l = coordinates.length; i < l; i++) {
                            edges.push({
                                type: 'LineString',
                                coordinates: [coordinates[i], coordinates[i - 1]]
                            });
                        }
                    });
                });

                let routeObjects = window.ymaps.geoQuery(edges).addToMap(self.map);
                let objectsInCity = routeObjects.searchInside(self.cityPoly);
                let boundaryObjects = routeObjects.searchIntersect(self.cityPoly);

                const results = routeObjects.remove(objectsInCity).remove(boundaryObjects);
                let dist = 0;

                results.each(function(path) {
                    dist += path.geometry.getDistance();
                });

                routeObjects.removeFromMap(self.map);

                self.props.updateData(((dist / 1000).toFixed(0)), distance);

                let mayPoint1 = self.multiRoute.getWayPoints().get(0).properties.getAll().address;
                let mayPoint2 = self.multiRoute.getWayPoints().get(1).properties.getAll().address;
                inputTo.current.value = mayPoint1;
                inputFrom.current.value = mayPoint2;
            });

            self.toAddr.destroy();
            self.fromAddr.destroy();
            self.addSuggest();
        });

        this.map.geoObjects.add(self.multiRoute);
    }

    render() {
        const defaultClass = 'YaMaps';

        return (
            <div className={defaultClass}>
                <div id="map" />
            </div>
        );
    }
}

export default YaMaps;
