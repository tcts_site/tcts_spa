import React, { PureComponent } from 'react';

class Contacts extends PureComponent {
    render() {
        const defaultClass = 'contacts';

        return (
            <div className={`${defaultClass}`}>
                <div className={`content-block`}>
                    <div className={`content-block__inner`}>
                        <h2 className={`content-block__title`}>Контакты</h2>
                        <div className={`content-block__text`}>
                            <h3 className={`headline headline_type-1`}>Номер телефона</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                <a className={`link link_underline`} href="tel:84952011591">+7 (495) 201-15-91</a>
                            </p>

                            <h3 className={`headline headline_type-1`}>Электронная почта</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                <a className={`link link_underline`} href="mailto:i9152981413@gmail.com">i9152981413@gmail.com</a>
                            </p>

                            <h3 className={`headline headline_type-1`}>Наш адрес</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                <a className={`link link_underline`} href="https://yandex.ru/maps/-/CBRjJ2QT8A" target="_blank">Москва, ул.Василия Петушкова, 8</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Contacts;
