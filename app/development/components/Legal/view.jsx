import React, { PureComponent } from 'react';

class Legal extends PureComponent {
    render() {
        const defaultClass = 'legal';

        return (
            <div className={`${defaultClass}`} id="legal">
                <div className={`content-block`}>
                    <div className={`content-block__inner`}>
                        <h2 className={`content-block__title`}>Грузоперевозки для юридических лиц</h2>
                        <div className={`content-block__text`}>
                            <h3 className={`headline headline_type-1`}>Что это?</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                Грузоперевозки для юридических лиц осуществляются по договору, в котором прописываются все условия и правила, не поодлежащие изменению после его подписания. Это позволяет гарантировать качественное и своевременное исполнение обязательств как с вашей, так и с нашей стороны.
                            </p>
                            <h3 className={`headline headline_type-1`}>Почему у нас?</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                Почему же стоит сотрудничать с нашей компанией? Приведем несколько преимуществ «Транстехсервис»:
                            </p>
                            <ul className={`card-list`}>
                                <li className={`card-list__item card-list__item_1to3 card-list__item_type-1`}>
                                    <h4 className={`headline headline_type-2`}>Фиксированная цена</h4>
                                    <p className={`paragraph paragraph_type-2`}>После подписания договора на оказание услуги "Грузоперевозки для юридических лиц" стоимость не будет изменяться, если в договор не будут внесены изменения со стороны заказчика</p>
                                </li>
                                <li className={`card-list__item card-list__item_1to3 card-list__item_type-1`}>
                                    <h4 className={`headline headline_type-2`}>Исполнение всех обязательств</h4>
                                    <p className={`paragraph paragraph_type-2`}>Обратившись к нам, вы можете не беспокоиться о том, что груз не будет доставлен в указанный срок, поскольку мы выполняем все обязательства, которые на себя возлагаем</p>
                                </li>
                                <li className={`card-list__item card-list__item_1to3 card-list__item_type-1`}>
                                    <h4 className={`headline headline_type-2`}>Бесплатные консультации</h4>
                                    <p className={`paragraph paragraph_type-2`}>Отзывчивые менеджеры готовы ответить на все ваши вопросы 24 часа в сутки</p>
                                </li>
                            </ul>
                            <h3 className={`headline headline_type-1`}>Что мы предлагаем?</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                Обеспечиваем предприятия грузовым транспортом. Осуществляем перевозки между структурными подразделениями и филиалами. Мы будем рады помочь вам в решении любой проблемы, связанной с транспортировкой мебели, бытовой техники, строительных материалов и других вещей. Для корпоративных клиентов у нас установлены специальные условия сотрудничества, которые мы можете уточнить у менеджеров компании, позвонив по указанным телефонам или оставить запрос на сайте компании.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Legal;
