import React, { PureComponent } from 'react';

class CargoTaxi extends PureComponent {
    render() {
        const defaultClass = 'cargo-taxi';

        return (
            <div className={`${defaultClass}`} id="taxi">
                <div className={`content-block`}>
                    <div className={`content-block__inner`}>
                        <h2 className={`content-block__title`}>Грузовое такси</h2>
                        <div className={`content-block__text`}>
                            <h3 className={`headline headline_type-1`}>Что такое грузовое такси?</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                Грузовое такси – это услуга, при помощи которой вы можете быстро и в то же время дешево, срочно перевести малогабаритный груз в пределах Москвы и Московской области.
                            </p>
                            <h3 className={`headline headline_type-1`}>В чем преимущество грузового такси?</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                Если рассматривать грузовое такси в Москве с другим видом доставки груза, оно имеет массу преимуществ, среди которых можно выделить:
                            </p>
                            <ul className={`card-list`}>
                                <li className={`card-list__item card-list__item_1to3 card-list__item_type-1`}>
                                    <h4 className={`headline headline_type-2`}>Оперативность</h4>
                                    <p className={`paragraph paragraph_type-2`}>Заказ грузотакси осуществляется непосредственно в момент, когда необходимо выполнить ранспортировку, то есть это идеальный вариант, если вам необходимо доставить вещи в кратчайшие сроки и нет возможности заказать услугу заранее</p>
                                </li>
                                <li className={`card-list__item card-list__item_1to3 card-list__item_type-1`}>
                                    <h4 className={`headline headline_type-2`}>Выгодная цена</h4>
                                    <p className={`paragraph paragraph_type-2`}>Для своих клиентов мы установили выгодные тарифы, которые позволяют экономить деньги, без ущерба качеству. Грузовое такси стоит дешево: цена на услуги начинаются от 650 руб. за 1 час, что совсем недорого для крупного мегаполиса</p>
                                </li>
                                <li className={`card-list__item card-list__item_1to3 card-list__item_type-1`}>
                                    <h4 className={`headline headline_type-2`}>Надежность</h4>
                                    <p className={`paragraph paragraph_type-2`}>В своей работе мы уделяем особое внимание сохранности груза во время транспортировки, поэтому вы можете не беспокоиться о том, что в процессе переезда вещи повредятся или потеряются. Мы гарантируем, что все будет доставлено в целости и сохранности</p>
                                </li>
                            </ul>
                            <h3 className={`headline headline_type-1`}>Что мы предлагаем?</h3>
                            <p className={`paragraph paragraph_type-1`}>
                                Мы оказываем услуги такси для перевозки грузов как частным лицам, так и крупным корпорациям. У нас разработаны выгодные тарифы, которые позволят недорого заказать транспортировку (перевозку) мебели техники вещей, строительных материалов, а также перевезти крупногабаритный и тяжелый груз. Стоимость грузового такси начинается от 650 рублей за 1 час работы. Более подробную информацию вам следует уточнить у менеджеров компании.
                            </p>
                            <h3 className={`headline headline_type-1`}>Как сделать заказ?</h3>
                            <div className={`order-block`}>
                                <div className={`card-list`}>
                                    <div className={`card-list__item card-list__item_1to3 card-list__item_type-2`}>
                                        <div className={`card`}>
                                            <div className={`card__image card__image_type-1`}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="49" height="89" viewBox="0 0 49 89">
                                                    <g fill="none" fillRule="evenodd">
                                                        <path d="M0 0h49v89H0z"/>
                                                        <path stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M39.67 79H9.33C3.32 79 1 76.68 1 70.67V17.33C1 11.32 3.32 9 9.33 9h30.35c6 0 8.32 2.32 8.32 8.33v53.35c0 6-2.32 8.32-8.33 8.32z"/>
                                                        <path stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9.78 15h29.45c2 0 2.78.77 2.78 2.78v4.45c0 2-.77 2.78-2.78 2.78H9.78C7.77 25 7 24.23 7 22.22v-4.45c0-2 .77-2.77 2.78-2.77z"/>
                                                        <circle cx="10.5" cy="35.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <circle cx="24.5" cy="35.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <circle cx="38.5" cy="35.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <circle cx="10.5" cy="49.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <circle cx="24.5" cy="49.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <circle cx="38.5" cy="49.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <circle cx="10.5" cy="63.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <circle cx="24.5" cy="63.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <circle cx="38.5" cy="63.5" r="3.5" stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
                                                        <path stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M7 20h26"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <p className={`paragraph paragraph_type-3`}>
                                                Рассчитать стоимость на сайте
                                            </p>
                                        </div>
                                    </div>
                                    <div className={`card-list__item card-list__item_1to3 card-list__item_type-2`}>
                                        <div className={`card`}>
                                            <div className={`card__image card__image_type-1`}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="48px" height="48px" viewBox="0 0 48 48" version="1.1">
                                                    <g transform="translate(-16.000000, -16.000000) translate(16.000000, 16.000000)" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                                        <polygon points="0 0 48 0 48 48 0 48"/>
                                                        <path d="M39.438,12.641 C38.293,8.371 35.275,5.349 33.038,4.057 L32.02,3.469 C31.216861,3.00360367 30.2613088,2.87763634 29.365,3.119 C28.4671627,3.35683283 27.701901,3.94419367 27.24,4.75 L23.977,10.4 C23.5116535,11.2032292 23.3867171,12.1591552 23.63,13.055 C23.872,13.96 24.45,14.715 25.257,15.181 L28.082,16.812 C29.475,17.616 29.884,18.924 29.368,20.928 C28.902,22.742 27.772,24.699 26.865,26.271 L27.73,26.771 C28.668,25.146 29.836,23.124 30.336,21.177 C30.962,18.74 30.372,16.98 28.581,15.946 L25.756,14.315 C25.1819135,13.9837577 24.7639416,13.4369021 24.595,12.796 C24.4207548,12.1564913 24.5093232,11.4738669 24.841,10.9 L28.104,5.25 C28.4344389,4.67439725 28.9814063,4.25489953 29.623,4.085 C30.263007,3.91150447 30.9458384,4.00154026 31.519,4.335 L32.537,4.922 C34.609,6.118 37.406,8.925 38.471,12.899 C39.597,17.103 39.25,21.518 34.05,30.527 C28.849,39.535 25.198,42.043 20.993,43.17 C17.017,44.235 13.191,43.215 11.119,42.02 L10.101,41.433 C9.52568258,41.1027076 9.10623998,40.556172 8.936,39.915 C8.76260485,39.2747695 8.8522521,38.5917942 9.185,38.018 L12.448,32.367 C12.7784389,31.7913972 13.3254063,31.3718995 13.967,31.202 C14.607007,31.0285045 15.2898384,31.1185403 15.863,31.452 L18.688,33.083 C20.48,34.117 22.298,33.749 24.095,31.988 C25.531,30.581 26.793,28.396 27.73,26.771 L27.2827454,26.5439453 L26.865,26.271 C25.958,27.843 24.734,29.962 23.395,31.273 C21.917,32.721 20.582,33.021 19.188,32.216 L16.363,30.585 C15.5602264,30.1185316 14.6042338,29.9925062 13.708,30.235 C12.8104362,30.4727977 12.0452735,31.0597226 11.583,31.865 L8.32,37.518 C7.85304412,38.3209728 7.72699954,39.2774712 7.97,40.174 C8.20783283,41.0718373 8.79519367,41.837099 9.601,42.299 L10.619,42.886 C12.198,43.798 14.718,44.615 17.565,44.615 C18.752,44.615 19.996,44.472 21.253,44.136 C25.704,42.943 29.542,40.334 34.915,31.027 C40.288,21.72 40.629,17.092 39.438,12.641 Z" fill="#EF3124" fillRule="nonzero"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <p className={`paragraph paragraph_type-3`}>
                                                Уточнить вопросы у оператора
                                            </p>
                                        </div>
                                    </div>
                                    <div className={`card-list__item card-list__item_1to3 card-list__item_type-2`}>
                                        <div className={`card`}>
                                            <div className={`card__image card__image_type-1`}>
                                                <svg style={{marginLeft: "9px"}} xmlns="http://www.w3.org/2000/svg" width="70" height="89" viewBox="0 0 70 89">
                                                    <g fill="none" fillRule="evenodd">
                                                        <path d="M0 0h70v89H0z"/>
                                                        <path stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M33.9 79H9.33C3.32 79 1 76.68 1 70.67V17.33C1 11.32 3.32 9 9.33 9h37.35c6 0 8.32 2.32 8.32 8.33V59.4"/>
                                                        <path stroke="#EF3124" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M37.54 66.22L48.81 80.1c1.06 1.31 2.51 1.16 3.32-.27l16.29-28.51M15 25h26M15 37h26M15 49h26"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <p className={`paragraph paragraph_type-3`}>
                                                Оформить заказ
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CargoTaxi;
