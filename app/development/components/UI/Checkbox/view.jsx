import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class Checkbox extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            focused: false,
            hovered: false,
            checked: false
        };
        this.refCheckbox = React.createRef();
    }

    render() {
        const { className, id, size, position } = this.props;
        const defaultClass = 'checkbox';

        const checkboxClasses = classNames(
            defaultClass,
            this.props.checked && `${defaultClass}__checked`,
            size &&`${defaultClass}_size_${size}`,
            position &&`${defaultClass}_position_${position}`,
            className
        );
        const checkboxProps = {
            className: checkboxClasses,
            id,
            htmlFor: id,
            ref: this.refCheckbox,
            onFocus: this.handleFocus.bind(this),
            onBlur: this.handleBlur.bind(this),
            onMouseEnter: this.handleMouseEnter.bind(this),
            onMouseLeave: this.handleMouseLeave.bind(this),
        };

        let checked = this.props.checked !== null
            ? this.props.checked
            : this.state.checked;

        return (
            <label {...checkboxProps}>
                {
                    this.props.type === 'button'
                        ? this.renderButtonCheckbox(defaultClass, checked)
                        : this.renderNormalCheckbox(defaultClass, checked)
                }
            </label>
        );
    }

    renderButtonCheckbox(defaultClass, checked) {
        return null;
    }

    renderNormalCheckbox(defaultClass, checked) {
        const { name, id, disabled, text } = this.props;

        const boxProps = {
            className: `${defaultClass}__box`,
            key: 'box',
        };

        const inputProps = {
            className: `${defaultClass}__control`,
            type: 'checkbox',
            autoComplete: 'off',
            name: name,
            id: id,
            checked: checked,
            disabled: disabled,
            onClick: this.handleInputControlClick.bind(this),
            onChange: this.handleChange.bind(this),
        };

        const textProps = {
            className: `${defaultClass}__text`,
            key: 'text',
            rol: 'presentation',
        };

        return [
            <span {...boxProps}>
                <input {...inputProps} />
                { checked && <span className="icon icon_size_m icon_name_check checkbox__icon" /> }
            </span>,

            text && <span {...textProps}>{ text }</span>
        ];
    }

    handleInputControlClick(event) {
        event.stopPropagation();
    }

    handleChange() {
        const { disabled, onChange, value } = this.props;

        if (!disabled) {
            let nextCheckedValue = !(
                this.props.checked !== null
                    ? this.props.checked
                    : this.state.checked
            );

            this.setState({ checked: nextCheckedValue });

            if (onChange) {
                onChange(nextCheckedValue, value);
            }
        }
    }

    handleFocus(event) {
        this.setState({ focused: true });

        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    }

    handleBlur(event) {
        this.setState({ focused: false });

        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    }

    handleMouseEnter(event) {
        if (!this.props.disabled) {
            this.setState({ hovered: true });
        }

        if (this.props.onMouseEnter) {
            this.props.onMouseEnter(event);
        }
    }

    handleMouseLeave(event) {
        if (!this.props.disabled) {
            this.setState({ hovered: false });
        }

        if (this.props.onMouseLeave) {
            this.props.onMouseLeave(event);
        }
    }

    // focus() {
    //     this.root.focus();
    // }
    //
    // blur() {
    //     if (document.activeElement) {
    //         document.activeElement.blur();
    //     }
    // }

}

Checkbox.defaultProps = {
    className: null,
    id: null,
    type: 'normal',
    checked: null,
    name: null,
    value: null,
    disabled: false,
    text: null,
    size: null,
    onChange: null,
    onFocus: null,
    onBlur: null,
    onMouseEnter: null,
    onMouseLeave: null,
    position: null,
};

Checkbox.propTypes = {
    /** Идентификатор компонента в DOM */
    id: PropTypes.string,
    /** Дополнительный класс */
    className: PropTypes.string,
    /** Тип чекбокса */
    type: PropTypes.oneOf(['normal', 'button']),
    /** Имя компонента в DOM */
    name: PropTypes.string,
    /** Значение чекбокса, которое будет отправлено на сервер, если он выбран */
    value: PropTypes.string,
    /** Управление возможностью изменять состояние 'checked' компонента */
    disabled: PropTypes.bool,
    /** Управление состоянием вкл/выкл компонента */
    checked: PropTypes.bool,
    /** Текст подписи к чекбоксу */
    text: PropTypes.string,
    /** Размер компонента */
    size: PropTypes.string,
    /** Позиция компонента */
    position: PropTypes.string,
    /**
     * Обработчик изменения значения 'checked' компонента, принимает на вход isChecked и value компонента
     * @param {boolean} isChecked
     * @param {string} value
     */
    onChange: PropTypes.func,
    /**
     * Обработчик фокуса комнонента
     * @param {React.FocusEvent} event
     */
    onFocus: PropTypes.func,
    /**
     * Обработчик снятия фокуса компонента
     * @param {React.FocusEvent} event
     */
    onBlur: PropTypes.func,
    /**
     * Обработчик события наведения курсора на чекбокс
     * @param {React.MouseEvent} event
     */
    onMouseEnter: PropTypes.func,
    /**
     * Обработчик события снятия курсора с чекбокса
     * @param {React.MouseEvent} event
     */
    onMouseLeave: PropTypes.func
};

export default Checkbox;