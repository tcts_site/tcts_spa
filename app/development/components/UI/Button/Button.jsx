import React from 'react';

// да это кнопука, детка! И она по ней можно нажать!
const Button = props => (
    <button
        className={`button button_size${props.size}`}
        onClick={!props.loading ? props.clickHandler : null}
        type={props.type}
    >
        {props.loading ? 'Загрузка' : props.title}
    </button>
);

export default Button;