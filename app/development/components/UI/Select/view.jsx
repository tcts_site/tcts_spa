import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class Select extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            opened: false,
            value: props.value || [],
        };
        this.defaultClass = 'select';
        this.refButton = React.createRef();
    }

    componentDidMount() {
        this.props.options && this.selectFirstOption();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ value: nextProps.value });
    }

    selectFirstOption() {
        const { options } = this.props;
        const firstOption = this.getFirstOption(options);

        this.handleOptionCheck(firstOption);
    }

    getFirstOption(options) {
        const firstOption = options[0];

        if (firstOption.type) {
            return this.getFirstOption(firstOption.content);
        }

        return firstOption;
    }

    handleOptionCheck(firstOption) {
        this.setState({ value: firstOption.text });

        if (this.props.onChange) {
            this.props.onChange(firstOption);
        }
    }

    handleMouseDown(value) {
        const { onChange } = this.props;

        this.setState({ value: value.text });
        if (onChange) {
            onChange(value);
        }
    }

    handleFocus(event) {
        const { onFocus } = this.props;
        this.setState({ opened: true });

        if (onFocus) {
            onFocus(event);
        }
    }

    handleBlur(event) {
        const { onBlur } = this.props;
        this.setState({ opened: false });

        if (onBlur) {
            onBlur(event);
        }
    }

    renderOptions() {
        const { options } = this.props;
        const { opened } = this.state;
        const optionsClasses = classNames(
            `${this.defaultClass}-options`,
            opened && `${this.defaultClass}-options_visible`
        );

        const optionsProps = {
            className: optionsClasses,
        };

        return (
            <div {...optionsProps} key="options">
                {options.map((item, index) => (
                    <button
                        type="button"
                        key={index}
                        className="select-options__btn"
                        onMouseDown={() => this.handleMouseDown(item)}
                        value={item.text}
                    >
                        {item.text}
                    </button>
                ))}
            </div>
        );
    }

    renderButton() {
        const { value, opened } = this.state;
        const { icon } = this.props;
        const buttonClasses = classNames(
            `${this.defaultClass}-button`,
            opened && `${this.defaultClass}-button_visible`
        );
        const buttonProps = {
            className: buttonClasses,
            role: 'button',
        };

        return (
            <button
                type="button"
                key="button"
                ref={this.refButton}
                {...buttonProps}
                onFocus={this.handleFocus.bind(this)}
                onBlur={this.handleBlur.bind(this)}
            >
                <span className="select-button__text">{value}</span>
                {icon && <span className="select-button__icon" />}
            </button>
        );
    }

    render() {
        const { className, id, label, options } = this.props;
        const { value } = this.state;

        const selectClasses = classNames(this.defaultClass, className);
        const selectProps = {
            className: selectClasses,
            id,
        };

        const labelClasses = classNames({
            'has-label': !!label,
        });
        const labelProps = { className: labelClasses };

        return (
            <div {...selectProps}>
                <div className="select__inner">
                    <input type="hidden" value={value} key="input" />
                    { !!label && <label {...labelProps} key="label">{ label }</label> }
                    { this.renderButton()}
                    { options && this.renderOptions()}
                </div>
            </div>
        );
    }
}

Select.defaultProps = {
    options: null,
    className: null,
    id: null,
    onFocus: null,
    onBlur: null,
    onMouseDown: null,
    label: null,
    value: null,
    icon: false
};

Select.propTypes = {
    /** Уникальный идентификатор блока */
    id: PropTypes.string,
    /** Дополнительный класс */
    className: PropTypes.string,
    /** Список вариантов выбора */
    options: PropTypes.arrayOf(
        PropTypes.shape({
            /** Уникальное значение, которое будет отправлено на сервер, если вариант выбран */
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            /** Текст варианта */
            text: PropTypes.node,
        })
    ),
    /** Обработчик фокуса на компоненте */
    onFocus: PropTypes.func,
    /** Обработчик потери фокуса компонентом */
    onBlur: PropTypes.func,
    /** Обработчик клика по кнопке компонента */
    onMouseDown: PropTypes.func,
    /** Описание для компонента */
    label: PropTypes.string,
    /** Иконка для компонента */
    icon: PropTypes.bool,
    /** Значение для компонента */
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default Select;