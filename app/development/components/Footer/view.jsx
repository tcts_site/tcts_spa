import React from 'react';
import { NavLink } from 'react-router-dom';
import { LayoutContext } from "../../layouts/LayoutMain/view";

export default class Footer extends React.Component {
    static contextType = LayoutContext;

    handleClick() {
        this.context.toogleFeedback();
    }
    render() {
        return (
            <div className="Footer">
                <div className="Footer__content">
                    <div className="Footer__inner">
                        <div className="Footer__item">
                            <div className="Footer__itemTitle">
                                Наш адрес:<br/> <a className={`link link_underline`} href="https://yandex.ru/maps/-/CBRjJ2QT8A" target="_blank">Москва, ул.Василия Петушкова, 8</a>
                            </div>
                            <div className="Footer__itemSubtitle">
                                <NavLink className={`link`} to="/contacts/">Контакты</NavLink>
                            </div>
                        </div>
                        <div className="Footer__item">
                            <div className="Footer__itemTitle">
                                Телефон: <a className={`link link_underline`} href="tel:84952011591">+7 (495) 201-15-91</a><br/>
                                Email: <a className={`link link_underline`} href="mailto:i9152981413@gmail.com">i9152981413@gmail.com</a>
                            </div>
                            <div className="Footer__itemSubtitle">
                                <NavLink className={`link`} to="#" onClick={this.handleClick.bind(this)}>
                                    Обратный звонок
                                </NavLink>
                            </div>
                        </div>
                        <div className="Footer__item">
                            <div className="Footer__itemTitle">
                                <NavLink className={`link link_underline`} to="/privacy/">
                                    Политика обработки <br/>персональных данных
                                </NavLink>
                            </div>
                            <div className="Footer__itemSubtitle">
                                <NavLink className={`link`} to="/about/">Компания “ТрансТехСервис”</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}