import React, { PureComponent } from 'react';
import classNames from 'classnames';

export default class Feedback extends PureComponent {
    constructor(props) {
        super(props);
        this.inputYourName = React.createRef();
        this.inputYourPhone = React.createRef();
        this.buttonSend = React.createRef();
        this.state = {
            fadeIn: null,
            toggleTooltip: {
                yourName: false,
                yourPhone: false,
            },
            isLoading: false,
            isSuccess: false,
        }
    }

    componentDidMount() {
        this.setState({
            fadeIn: 'fadeInStart',
            fadeOut: null,
        });
        setTimeout(() => {
            this.setState({
                fadeIn: 'fadeInEnd',
            });
        }, 100);
    }

    closeModal = () => {
        this.setState({
            fadeIn: 'fadeOutStart',
            isSuccess: false,
            isLoading: false,
        });
        setTimeout(() => {
            this.setState({
                fadeIn: 'fadeOutEnd',
            });
            this.props.toogleFeedback();
        }, 100);
    };

    sendMail = () => {
        if (!this.state.isLoading) {
            const _this = this;
            fetch("http://localhost:8080/api/backend/feedback/send", {
                cors: true,
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: _this.inputYourName.current.value,
                    phone: _this.inputYourPhone.current.value,
                    type: 'feedback',
                })
            })
                .then(res => res.json())
                .then(res => {
                    _this.setState({
                        isSuccess: true,
                    });
                });

            this.setState({
                isLoading: true,
            });
        }
    };

    handlerClick = () => {
        const { inputYourName, inputYourPhone } = this;
        const inputs = [
            inputYourName,
            inputYourPhone
        ];

        const hasValue = input => input.current.value.length === 0;
        const successData = inputs.filter(hasValue);
        inputs.forEach(input => input.current.classList.remove('feedback__input_invalid'));

        if (successData.length === 0) {
            this.sendMail();
        } else {
            successData.forEach(input => input.current.classList.add('feedback__input_invalid'));
        }
    };

    handlerChange = () => {
        const { inputYourName, inputYourPhone } = this;
        const inputs = [
            inputYourName,
            inputYourPhone
        ];

        const noValue = input => input.current.value.length !== 0;

        const successData = inputs.filter(noValue);
        successData.forEach(input => input.current.classList.remove('feedback__input_invalid'));
    };

    handlerFocus = event => {
        const hasTooltip = event.target.classList.contains('feedback__input_invalid');
        const name = event.target.name;

        if (hasTooltip) {
            this.setState({
                toggleTooltip: {
                    [name]: hasTooltip,
                },
            });
        }
    };

    handlerBlur = event => {
        const name = event.target.name;

        this.setState({
            toggleTooltip: {
                [name]: false,
            },
        });
    };

    render() {
        const defaultClass = 'feedback';
        const { isLoading, isSuccess } = this.state;
        const inputYourName = {
            key: 'inputYourName',
            className: `${defaultClass}__input ${defaultClass}__input_your-name`,
            id: 'your-name',
            type: 'text',
            placeholder: 'Как Вас зовут',
            ref: this.inputYourName,
            name: 'yourName',
            autoComplete: "off",
            onChange: this.handlerChange,
            onFocus: this.handlerFocus,
            onBlur: this.handlerBlur,
        };

        const inputYourPhone = {
            key: 'inputYourPhone',
            className: `${defaultClass}__input ${defaultClass}__input_your-phone`,
            id: 'your-phone',
            type: 'number',
            placeholder: 'Номер Вашего телефон',
            ref: this.inputYourPhone,
            name: 'yourPhone',
            autoComplete: "off",
            onChange: this.handlerChange,
            onFocus: this.handlerFocus,
            onBlur: this.handlerBlur,
        };

        const buttonSend = {
            key: 'buttonSend',
            className: `${defaultClass}__button ${defaultClass}__button_send`,
            id: 'button_send',
            ref: this.buttonSend,
            name: 'button_send',
        };

        const classProps = classNames(
            defaultClass,
            this.state.fadeIn && this.state.fadeIn,
        );

        return (
            <div className={classProps}>
                <div className={`${defaultClass}__overlay`} onClick={this.closeModal.bind(this)} />
                <div className={`${defaultClass}__block`}>
                    {
                        !isSuccess
                            ? <div className={`${defaultClass}__inner`}>
                                <p className={`${defaultClass}__header`}>Обратный звонок</p>
                                <div className={`fields`}>
                                    <div className={`field`}>
                                        <input {...inputYourName} />
                                        {this.state.toggleTooltip.yourName && <span className="tooltip">Заполните это поле</span>}
                                    </div>
                                    <div className={`field mt-15`}>
                                        <input {...inputYourPhone} />
                                        {this.state.toggleTooltip.yourPhone && <span className="tooltip">Заполните это поле</span>}
                                    </div>
                                    <div className={`field mt-15`}>
                                        <button {...buttonSend} onClick={this.handlerClick}>
                                            <span className={isLoading ? "loading" : null}>{isLoading ? "" : "Отправить"}</span>
                                        </button>
                                    </div>
                                </div>
                                <div className={`${defaultClass}__close`} onClick={this.closeModal} />
                            </div>

                            : <div className={`${defaultClass}__inner`}>
                                <div className={`${defaultClass}__message_success`}>Ваша заявка принята!</div>
                                <div className={`${defaultClass}__close`} onClick={this.closeModal} />
                            </div>
                    }
                </div>
            </div>
        );
    }
}
