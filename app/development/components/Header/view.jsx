import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => (
    <header className="Header-root">
        <div className="Header-logo">
            <NavLink to="/" className="Header-logoTCTS">
                <svg width="80" height="80" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <rect x="48" y="31" width="16" height="4" fill="white"/>
                    <rect x="48" y="45" width="18" height="4" fill="white"/>
                    <rect y="45" width="46" height="4" fill="white"/>
                    <rect y="31" width="46" height="4" fill="white"/>
                    <rect y="25" width="46" height="4" fill="white"/>
                    <rect y="38" width="46" height="4" fill="white"/>
                    <rect y="51" width="72" height="4" fill="white"/>
                    <rect x="48" y="49" width="18" height="4" transform="rotate(-90 48 49)" fill="white"/>
                    <path d="M67.931 46L62 35.1745L64 31L72 45.1509L67.931 46Z" fill="white"/>
                    <rect x="68" y="55" width="10" height="4" transform="rotate(-90 68 55)" fill="white"/>
                    <rect width="80" height="4" fill="white"/>
                    <rect y="76" width="80" height="4" fill="white"/>
                    <rect x="76" y="80" width="80" height="4" transform="rotate(-90 76 80)" fill="white"/>
                    <rect y="29" width="29" height="4" transform="rotate(-90 0 29)" fill="white"/>
                    <rect y="80" width="29" height="4" transform="rotate(-90 0 80)" fill="white"/>
                </svg>
            </NavLink>
        </div>
        <div className="Header-nav">
            <nav className="Header-menu">
                <NavLink to="/about" className="Header-item Header-itemMenu">О нас</NavLink>
                <NavLink to="/services" className="Header-item Header-itemMenu">Услуги</NavLink>
                <NavLink to="/contacts" className="Header-item Header-itemMenu">Контакты</NavLink>
            </nav>
        </div>
        <div className="Header-buttons">
            <a href="tel:84952011591" className="Header-item Header-itemPhone">+7 (495) 201-15-91</a>
        </div>
    </header>
);

export default Header;
