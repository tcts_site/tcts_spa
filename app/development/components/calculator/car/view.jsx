import React, { PureComponent } from 'react';

class Car extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    handlerClick = event => {
        this.props.handlerNext(Number(event.target.value));
    };

    checkCar = (event) => {
        this.props.checkCars(this.props.priceData[0].Data[event.target.id]);
    };

    render() {
        const defaultClass = 'cars';

        return (
            <div className={`${defaultClass}`} style={{ display: `${this.props.tabs === 2 ? 'flex' : 'none'}` }}>
                <div className={`fields`}>
                    <div className={`field mx-15`}>
                        <div className={`field__title`}>2. Выберите тип автомобиля:</div>
                    </div>
                    <div className={`field mb-15 mr-15 ml-15`}>
                        <div className="cars">
                            <div className="cars__list">
                                {this.props.priceData.length > 0 && this.props.priceData[0].Data.map((price, index) => (
                                    <div className="cars__item">
                                        <input type="radio" name='car' id={index} onChange={this.checkCar} />
                                        <label htmlFor={index} className="cars__card">
                                        <span className="cars__icon">
                                            <img alt="" height="30" src="https://msk.gruzovichkof.ru/uploads/autopark/icon_msk-1459333831-gazelle_fermer_tent-3m-1,5t.png" className="car-cover" />
                                        </span>
                                            <span className="cars__main">
                                            <span>Грузоподъемность: </span>
                                            <span>до {price.Volume} тоннн</span>
                                        </span>
                                            <span className="cars__main">
                                            <span>Объем кузова: </span>
                                            <span>до {price.Capacity} куб.м</span>
                                        </span>
                                            <div className="cars__overlay" />
                                        </label>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`fields`}>
                    <div className={`field mr-15 ml-15 mt-auto`}>
                        <div className="btn-container">
                            <button className="prev-link" value="1" onClick={this.handlerClick}>← Назад</button>
                            <button className={`next-link`} value="3" onClick={this.handlerClick}>Продолжить</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Car;
