import React, { PureComponent } from 'react';

import Checkbox from './../../UI/Checkbox';
import Select from './../../UI/Select';

class ExtraOptions extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pallet: null,
            time: null,
        }
    }

    getSelect = value => {
        this.props.checkForms(value);
    };

    handlerClick = event => {
        this.props.handlerNext(Number(event.target.value));
    };

    render() {
        const defaultClass = 'extra-options';

        return (
            <div className={`${defaultClass}`} style={{ display: `${this.props.tabs === 3 ? 'flex' : 'none'}` }}>
                {this.props.extra && (
                    <div className={`fields`}>
                        <div className={`field mx-15`}>
                            <div className={`field__title`}>3. Дополнительные услуги:</div>
                        </div>
                        <div className={`field mx-15`}>
                            <Select
                                value={this.props.time.text}
                                options={this.props.extra.time}
                                label="Время заказа"
                                onChange={this.getSelect}
                            />
                        </div>
                        <div className={`field mx-15`}>
                            <Select
                                value={this.props.pallet.text}
                                options={this.props.extra.pallet}
                                label="Кол. европаллета 120х80 (шт)"
                                onChange={this.getSelect}
                            />
                        </div>
                        <div className={`field mx-15`}>
                            <Select
                                icon
                                value={this.props.curtains.text}
                                options={this.props.extra.curtains}
                                label="Растентовка ТС"
                                onChange={this.getSelect}
                            />
                        </div>
                        <div className={`field mx-15`}>
                            <Checkbox size="l" text="с НДС" onChange={this.getSelect} />
                        </div>
                    </div>
                )}
                <div className={`fields`}>
                    <div className={`field mr-15 ml-15 mt-auto`}>
                        <div className="btn-container">
                            <button className="prev-link" value="2" onClick={this.handlerClick}>← Назад</button>
                            <button className={`next-link`} value="4" onClick={this.handlerClick}>Продолжить</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ExtraOptions;
