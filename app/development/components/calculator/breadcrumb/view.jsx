import React, { PureComponent } from 'react';

class BreadCrumbs extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    handlerClick = event => {
        this.props.handlerNext(Number(event.target.dataset.value));
    };

    render() {
        const { tabs } = this.props;

        return (
            <ul className="publication-breadcrumb">
                { tabs >= 1 && (
                    <li className={`step1 ${tabs === 1 ? 'current' : ''}`}>
                        <span data-value="1" onClick={this.handlerClick}>Маршрут</span>
                    </li>
                )}
                { tabs >= 2 && (
                    <li className={`step2 ${tabs === 2 ? 'current' : ''}`}>
                        <span data-value="2" onClick={this.handlerClick}>Транспорт</span>
                    </li>
                )}
                { tabs >= 3 && (
                    <li className={`step3 ${tabs === 3 ? 'current' : ''}`}>
                        <span data-value="3" onClick={this.handlerClick}>Дополнение</span>
                    </li>
                )}
                { tabs >= 4 && (
                    <li className={`step4 ${tabs === 4 ? 'current' : ''}`}>
                        <span data-value="4" onClick={this.handlerClick}>Результат</span>
                    </li>
                )}
            </ul>
        );
    }
}

export default BreadCrumbs;
