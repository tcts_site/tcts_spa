import React, { PureComponent } from 'react';

class FormResult extends PureComponent {
    constructor(props) {
        super(props);
        this.name = React.createRef();
        this.phone = React.createRef();
        this.state = {
            toggleTooltip: {
                toAddress: false,
                fromAddress: false,
            },
        }
    }

    handlerChange = () => {
        const { name, phone } = this;
        const inputs = [
            name,
            phone
        ];

        const noValue = input => input.current.value.length !== 0;

        const successData = inputs.filter(noValue);
        successData.forEach(input => input.current.classList.remove('input_invalid'));
    };

    handlerFocus = event => {
        const hasTooltip = event.target.classList.contains('input_invalid');
        const name = event.target.name;

        if (hasTooltip) {
            this.setState({
                toggleTooltip: {
                    [name]: hasTooltip,
                },
            });
        }
    };

    handlerBlur = event => {
        const name = event.target.name;

        this.setState({
            toggleTooltip: {
                [name]: false,
            },
        });
    };

    handlerClick = event => {
        this.props.handlerNext(Number(event.target.value));
    };

    render() {
        const defaultClass = 'form-result';
        const inputName = {
            key: 'inputToAddress',
            className: `input input_name`,
            id: 'address_to',
            type: 'text',
            ref: this.name,
            placeholder: 'Ф.И.О',
            onChange: this.handlerChange,
            onFocus: this.handlerFocus,
            onBlur: this.handlerBlur,
            name: 'name',
        };

        const inputPhone= {
            key: 'inputFromAddres',
            className: `input input_phone`,
            id: 'address_from',
            type: 'number',
            ref: this.phone,
            placeholder: 'Номер телефона',
            onChange: this.handlerChange,
            onFocus: this.handlerFocus,
            onBlur: this.handlerBlur,
            name: 'phone',
        };

        const mkad_result = this.props.car ? (~~(Number(this.props.distance.value) / 20) + 1 + (this.props.car.Capacity > 10 ? (~~(Number(this.props.distance.value) / 20) + 1) * 0.5 : 0)) : 0;
        const nds = this.props.nds ? (this.props.car.Hour * this.props.time.value) / 100 * 20 : 0;
        const summar = this.props.car ? ((this.props.time.value + mkad_result) * this.props.car.Hour + nds + this.props.curtains.value) : 0;

        return (
            <div className={`${defaultClass}`} style={{ display: `${this.props.tabs === 4 ? 'flex' : 'none'}` }}>
                <div className={`fields`}>
                    <div className={`field mx-15`}>
                        <div className={`field__title`}>4. Результат рассчета:</div>
                    </div>
                    <div className={`field mb-15 mr-15 ml-15`}>
                        {this.props.car && (<div className={`${defaultClass}__total`}>
                            {summar} рублей
                        </div>)}
                    </div>
                    <div className={`field mx-15`}>
                        <div className={`field__title`}>Заказ услуги:</div>
                    </div>
                    <div className={`field mb-15 mr-15 ml-15`}>
                        <label className="label" htmlFor="address_to">Ваши Ф.И.О</label>
                        <input {...inputName} />
                        {this.state.toggleTooltip.toAddress && <span className="tooltip">Заполните это поле</span>}
                    </div>
                    <div className={`field mb-15 mr-15 ml-15`}>
                        <label className="label" htmlFor="address_from">Телефон</label>
                        <input {...inputPhone} />
                        {this.state.toggleTooltip.fromAddress && <span className="tooltip">Заполните это поле</span>}
                    </div>
                    <div className={`field mt-35 mr-15 ml-auto`}>
                        <div className="btn-container">
                            <button className="prev-link" value="3" onClick={this.handlerClick}>← Назад</button>
                            <button className={`next-link`}>Заказать</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FormResult;
