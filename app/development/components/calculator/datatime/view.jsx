import React, { PureComponent } from 'react';

class DataTime extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    handlerClick = event => {
        this.props.handlerNext(Number(event.target.value));
    };

    render() {
        const defaultClass = 'datatime';

        return (
            <div className={`${defaultClass}`} style={{ display: `${this.props.tabs === 3 ? 'flex' : 'none'}` }}>
                <div className={`fields`}>
                    <div className={`field mx-15`}>
                        <div className={`field__title`}>Укажите время работы:</div>
                    </div>
                </div>
                <div className={`fields`}>
                    <div className={`field mr-15 ml-15 mt-auto`}>
                        <div className="btn-container">
                            <button className="prev-link" value="2" onClick={this.handlerClick}>← Назад</button>
                            <button className={`next-link`} value="4" onClick={this.handlerClick}>Продолжить</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DataTime;
