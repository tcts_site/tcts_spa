import React, { PureComponent } from 'react';

import YaMaps from '../../YaMaps';

class Total extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            text: null,
            addrs: null,
            tabs: 1,
        }
    }

    componentWillReceiveProps(p) {
        if (this.state.tabs < p.tabs) {
            this.setState({
                tabs: p.tabs,
            });
        }
    }

    updateData = (value, all) => {
        this.setState({
            addrs: {value, all}
        });

        this.props.mapDistance(value, all);
    };

    render() {
        const defaultClass = 'total';

        return (
            <div className={`${defaultClass}`}>
                <div className={`fields fields--maps`}>
                    <div className={`field mx-15`}>
                        <YaMaps
                            inputToAddress={this.props.inputToAddress}
                            inputFromAddress={this.props.inputFromAddress}
                            updateData={this.updateData}
                        />
                    </div>

                    {this.state.tabs >= 1 && this.state.addrs && (
                        <div className={`field mx-15`}>
                            <div className={`${defaultClass}__results-item`}>
                                <label>Расстояние:</label>
                                {this.state.addrs && <div>{this.state.addrs.all.distance.text}</div>}
                            </div>
                            <div className={`${defaultClass}__results-item`}>
                                <label>За МКАД:</label>
                                {this.state.addrs &&<div>{this.state.addrs.value} км</div>}
                            </div>
                            <div className={`${defaultClass}__results-item`}>
                                <label>Въезд в ТТК:</label>
                                {this.state.addrs &&<div>{this.state.addrs.value > 0 ? 'Да' : 'Нет'}</div>}
                            </div>
                            <div className={`${defaultClass}__results-item`}>
                                <label>Въезд в СК:</label>
                                {this.state.addrs &&<div>{this.state.addrs.value > 0 ? 'Да' : 'Нет'}</div>}
                            </div>
                        </div>
                    )}

                    {this.state.tabs >=  2 && this.props.car && (
                        <div className={`field mx-15`}>
                            <div className="car">
                            <span className="car__icon">
                                <img alt="" height="60" src="https://msk.gruzovichkof.ru/uploads/autopark/icon_msk-1459333831-gazelle_fermer_tent-3m-1,5t.png" className="car-cover" />
                            </span>
                                <span className="car__extend">
                                <span className="car__main">
                                    <span>Грузоподъемность: </span>
                                    {this.props.car &&<span>до {this.props.car.Volume} тоннн</span>}
                                </span>
                                <span className="car__main">
                                    <span>Объем кузова: </span>
                                    {this.props.car &&<span>до {this.props.car.Capacity} куб.м</span>}
                                </span>
                            </span>
                            </div>
                        </div>
                    )}

                    {this.state.tabs >= 3 && (
                        <div className={`field mx-15`}>
                            <div className={`${defaultClass}__results-item`}>
                                <label>Время заказа:</label>
                                {this.props.time && <div>{this.props.time.text} ч.</div>}
                            </div>
                            <div className={`${defaultClass}__results-item`}>
                                <label>Европаллет 120*80:</label>
                                {this.props.pallet && <div>{this.props.pallet.text} шт.</div>}
                            </div>
                            <div className={`${defaultClass}__results-item`}>
                                <label>Один час работы:</label>
                                {this.props.car && <div>{this.props.car.Hour} руб.</div>}
                            </div>
                            <div className={`${defaultClass}__results-item`}>
                                <label>Растентовка:</label>
                                {this.props.curtains && <div>{this.props.curtains.text}</div>}
                            </div>
                            <div className={`${defaultClass}__results-item`}>
                                <label>с НДС:</label>
                                {<div>{this.props.nds ? 'Да' : 'Нет'}</div>}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default Total;
