import React, { PureComponent } from 'react';

class Addresses extends PureComponent {
    constructor(props) {
        super(props);
        this.toAddr = React.createRef();
        this.fromAddr = React.createRef();
        this.state = {
            text: null,
            toggleTooltip: {
                toAddress: false,
                fromAddress: false,
            },
        }
    }

    componentDidMount() {
        this.props.createdRef(this.toAddr, this.fromAddr);
    }

    handlerChange = () => {
        const { toAddr, fromAddr } = this;
        const inputs = [
            toAddr,
            fromAddr
        ];

        const noValue = input => input.current.value.length !== 0;

        const successData = inputs.filter(noValue);
        successData.forEach(input => input.current.classList.remove('input_invalid'));
    };

    handlerFocus = event => {
        const hasTooltip = event.target.classList.contains('input_invalid');
        const name = event.target.name;

        if (hasTooltip) {
            this.setState({
                toggleTooltip: {
                    [name]: hasTooltip,
                },
            });
        }
    };

    handlerBlur = event => {
        const name = event.target.name;

        this.setState({
            toggleTooltip: {
                [name]: false,
            },
        });
    };

    handlerClick = event => {
        this.props.handlerNext(Number(event.target.value));
    };

    render() {
        const defaultClass = 'addresses';
        const inputToAddress = {
            key: 'inputToAddress',
            className: `input input_address_to`,
            id: 'address_to',
            type: 'text',
            placeholder: 'Введите адрес',
            ref: this.toAddr,
            onChange: this.handlerChange,
            onFocus: this.handlerFocus,
            onBlur: this.handlerBlur,
            name: 'toAddress',
        };

        const inputFromAddress= {
            key: 'inputFromAddres',
            className: `input input_address_from`,
            id: 'address_from',
            type: 'text',
            placeholder: 'Введите адрес',
            ref: this.fromAddr,
            onChange: this.handlerChange,
            onFocus: this.handlerFocus,
            onBlur: this.handlerBlur,
            name: 'fromAddress',
        };

        return (
            <div className={`${defaultClass}`} style={{ display: `${this.props.tabs === 1 ? 'flex' : 'none'}` }}>
                <div className={`fields`}>
                    <div className={`field mx-15`}>
                        <div className={`field__title`}>1. Выберите маршрут:</div>
                    </div>
                    <div className={`field mb-15 mr-15 ml-15`}>
                        <label className="label" htmlFor="address_to">Откуда</label>
                        <input {...inputToAddress} />
                        {this.state.toggleTooltip.toAddress && <span className="tooltip">Заполните это поле</span>}
                    </div>
                    <div className={`field mb-50 mr-15 ml-15`}>
                        <label className="label" htmlFor="address_from">Куда</label>
                        <input {...inputFromAddress} />
                        {this.state.toggleTooltip.fromAddress && <span className="tooltip">Заполните это поле</span>}
                    </div>
                </div>
                <div className={`fields`}>
                    <div className={`field mr-15 ml-15 mt-auto`}>
                        <div className="btn-container">
                            <button className={`next-link`} value="2" onClick={this.handlerClick}>Продолжить</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Addresses;
