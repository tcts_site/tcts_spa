import LayoutMain  from './layouts/LayoutMain';
import PageIndex from './pages/Index';
import PageServices from './pages/Services';
import PageAbout from './pages/About';
import PageContacts from './pages/Contacts';
import PageNotFound from './pages/NotFound';

const routes = [{
    component: LayoutMain,
    routes: [
        ...PageIndex,
        ...PageServices,
        ...PageAbout,
        ...PageContacts,
        ...PageNotFound
    ]
}];

export default routes;