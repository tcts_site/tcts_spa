import IndexController from "../controllers/Index.controller";

const routes = [
	{
		path: '/',
		exact: true,
		component: IndexController,
        actions: () => ([]),
	},
];

export default routes;
