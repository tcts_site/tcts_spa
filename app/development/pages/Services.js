import ServicesController from "../controllers/Services.controller";

const routes = [
	{
		path: '/services',
		exact: true,
		component: ServicesController,
        actions: () => ([]),
	},
];

export default routes;
