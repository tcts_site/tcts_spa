import NotFoundController from "../controllers/NotFound.controller";

const routes = [
	{
		path: '*',
		component: NotFoundController,
        actions: () => ([]),
	},
];

export default routes;
