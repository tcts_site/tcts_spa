import ContactsController from "../controllers/Contacts.controller";

const routes = [
	{
		path: '/contacts',
		exact: true,
		component: ContactsController,
        actions: () => ([]),
	},
];

export default routes;
