import AboutController from "../controllers/About.controller";

const routes = [
	{
		path: '/about',
		exact: true,
		component: AboutController,
        actions: () => ([]),
	},
];

export default routes;
