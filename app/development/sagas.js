import { all, fork } from 'redux-saga/effects';
import price from './containers/price/sagas';

export default function* RootSaga() {
    yield all([
        fork(price),
    ]);
}
