"use strict";

import core from '../core';
import Router from '../core/router';
import Settings from '../core/settings';

core.GetAppServer().AddModule(
    Settings.InitSettings,
    Router.CreateRouter,
);

core.GetAppServer().Run();
