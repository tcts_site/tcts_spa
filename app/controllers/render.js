import React from "react";
import { renderToString } from "react-dom/server";
import { Provider } from 'react-redux'
import { StaticRouter } from "react-router-dom";
import Helmet from "react-helmet";
import {applyMiddleware, createStore, compose} from "redux";
import createSagaMiddleware from "redux-saga";
import { END } from "redux-saga";

import models from "../development/containers/models";
import Application from "../development/app";
import ApplicationSaga from "../development/sagas";
import ApplicationReducer from "../development/store";
import ApiMiddleware from "../development/middlewares/api";

const syncAPI = (model, dispatch) => {
    const actions = model.actions();

    if (actions.length > 0) {
        actions.map(action => dispatch(action));
    }
};

const RenderHtml = (req, res, next) => {
    const model = models.filter(item => item.path === req.url).pop();
    const SagaMiddleware = createSagaMiddleware();
    const store = createStore(
        ApplicationReducer,
        compose(applyMiddleware(SagaMiddleware, ApiMiddleware)),
    );

    store.runSaga = SagaMiddleware.run;
    store.close = () => store.dispatch(END);

    const context = {};

    const rootComponent = (
        <Provider store={store}>
            <StaticRouter location={req.url} context={context}>
                <Application />
            </StaticRouter>
        </Provider>
    );

    store.runSaga(ApplicationSaga).done
        .then(() => {
            const preloadedState = store.getState();
            const html = renderToString(rootComponent);
            const meta = Helmet.rewind();

            if (context.status === 404) {
                res.status(404);
            }
            if (context.status === 302) {
                res.redirect(context.status, context.url);
            }
            res.render('index.ejs', { reactOutput: html, preloadedState: JSON.stringify(preloadedState), head: meta });
        })
        .catch((event) => {
            console.log("renderHtmlDocument failed:", event.message);
            next(event);
            res.status(500).send(event.message);
        });

    if (model) syncAPI(model, store.dispatch);

    store.close();
};

export default {
    RenderHtml,
};
