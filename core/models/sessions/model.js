"use strict";

import Mongo from '../../connectors/mongo';

const Init = () => {
    const Sessions = new Mongo.Connect().Schema({
        SessionBy: {
            type: String,
            unique: true,
        },
        AccessToken: String,
        RefreshToken: String,
        createAt: {
            type: Date,
            index: {
                expires: 60 * 60 * 5
            },
            default: Date.now
        }
    });

    Mongo.Connect().model('Sessions', Sessions)
};

export default {
    Init,
};
