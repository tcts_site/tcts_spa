"use strict";

import Mongo from '../../connectors/mongo';

const AddSession = (user, data) => {
    let session = {};
    let token = {};
    const sessionModel = Mongo.Connect().model("Sessions");
    const tokenModel = Mongo.Connect().model("Tokens");

    session.SessionBy = user.id;
    session.createAt = data.timestamp;
    session.RefreshToken = data.refreshToken;

    token.AccessToken = data.accessToken;

    return sessionModel.create(session)
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};

export default {
    AddSession,
};
