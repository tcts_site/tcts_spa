"use strict";

import Mongo from '../../connectors/mongo';

const Init = () => {
    const OAuthUsers = new Mongo.Connect().Schema({
        Active: {
            type: Boolean,
            default: false
        },
        Firstname: String,
        Lastname: String,
        Email: {
            type: String,
            required: true,
            unique: true,
        },
        Login: {
            type: String,
            required: true,
            unique: true,
        },
        Key: {
            type: String,
            required: true,
        },
        Group: {
            type: [String],
            default: ['edit']
        },
    });

    Mongo.Connect().model('OAuthUsers', OAuthUsers)
};

export default {
    Init,
};
