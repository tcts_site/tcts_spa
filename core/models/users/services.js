"use strict";

import Mongo from '../../connectors/mongo';

const AddUser = body => {
    let user = {};
    const model = Mongo.Connect().model("OAuthUsers");

    user.Active = body.active;
    user.Firstname = body.firstName;
    user.Lastname = body.lastName;
    user.Email = body.email;
    user.Login = body.login;
    user.Key = body.key;
    user.Group = body.group;

    return model.create(user)
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};

const GetUsersList = () => {
    const model = Mongo.Connect().model("OAuthUsers");

    return model.find({})
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};

const GetUser = body => {
    let { login } = body;
    const model = Mongo.Connect().model("OAuthUsers");

    return model.findOne({ Login: login })
        .then(data => {
            if (!data) throw new Error('User not found');
            return data
        }).catch(error => { throw error });
};

export default {
    AddUser,
    GetUsersList,
    GetUser,
};
