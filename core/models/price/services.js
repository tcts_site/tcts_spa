"use strict";

import Mongo from '../../connectors/mongo';

const GetPrice = () => {
    const model = Mongo.Connect().model("Price");

    return model.find({})
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};

const AddPrice = body => {
    let price = {
        Type: body.type,
        Data: body.data.map(item => ({
            Capacity: item.Capacity,
            Volume: item.Volume,
            Pallet: item.Pallet,
            Time: item.Time,
            Hour: item.Hour,
            Cost: item.Cost,
        })),
    };

    const model = Mongo.Connect().model("Price");

    return model.create(price)
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};

const UpdatePrice = body => {
    let priceType = body.type;
    let priceData = body.data.map(item => ({
        Capacity: item.Capacity,
        Volume: item.Volume,
        Pallet: item.Pallet,
        Time: item.Time,
        Hour: item.Hour,
        Cost: item.Cost,
    }));

    const model = Mongo.Connect().model("Price");

    return model.findOneAndUpdate({ Type: priceType }, {$set: { Data: priceData }}, { 'new': true })
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};

export default {
    GetPrice,
    AddPrice,
    UpdatePrice,
};
