"use strict";

import Mongo from '../../connectors/mongo';

const Init = () => {
    const Price = new Mongo.Connect().Schema({
        Type: {
            type: String,
        },
        Data: [{
            Capacity: {
                type: Number,
            },
            Volume: {
                type: Number,
            },
            Pallet: {
                type: Number,
            },
            Time: {
                type: Number,
            },
            Hour: {
                type: Number,
            },
            Cost: {
                type: Number,
            },
        }],
    });

    Mongo.Connect().model('Price', Price)
};

export default {
    Init,
};
