"use strict";

import Model from './model';
import Services from './services';

export default {
    ...Model,
    ...Services,
}
