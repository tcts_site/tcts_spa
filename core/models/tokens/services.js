"use strict";

import Mongo from '../../connectors/mongo';

const AddToken = (user, data) => {
    let token = {};
    const model = Mongo.Connect().model("Tokens");

    token.User_id = user.Login;
    token.createAt = data.timestamp;
    token.AccessToken = data.accessToken;
    token.RefreshToken = data.refreshToken;

    return model.create(token)
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};


const UpdateToken = (user, data) => {
    let token = {};
    const model = Mongo.Connect().model("Tokens");

    token.createAt = data.timestamp;
    token.AccessToken = data.accessToken;
    token.RefreshToken = data.refreshToken;

    return model.findOneAndUpdate({ User_id: user.Login }, { $set: token }, { 'new': true })
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};


const GetTokensList = () => {
    const model = Mongo.Connect().model("Tokens");

    return model.find({});
};


const GetToken = body => {
    let { login } = body;
    const model = Mongo.Connect().model("Tokens");

    return model.findOne({ User_id: login })
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};

const CheckToken = body => {
    let { login } = body;
    const model = Mongo.Connect().model("Tokens");

    return model.findOne({ User_id: login })
        .then(data => !data ? false : true)
        .catch(() => false);
};


const GetAccessToken = body => {
    let { login } = body;
    const model = Mongo.Connect().model("Tokens");

    return model.findOne({ User_id: login });
};


const GetRefreshToken = body => {
    let { token } = body;
    const model = Mongo.Connect().model("Tokens");

    return model.findOne({ RefreshToken: token });
};


const UpdateAccessToken = body => {
    let { login } = body;
    const model = Mongo.Connect().model("Tokens");

    return model.findOneAndUpdate({ User_id: login }, { AccessToken: '' });
};


const UpdateRefreshToken = body => {
    let { login } = body;
    const model = Mongo.Connect().model("Tokens");

    return model.findOneAndUpdate({ User_id: login }, { RefreshToken: '' });
};


const DeleteToken = body => {
    let { login } = body;
    const model = Mongo.Connect().model("Tokens");

    return model.findOneAndDelete({ User_id: login })
        .then(data => {
            if (!data) throw new Error('document not found');
            return data
        }).catch(error => { throw error });
};


export default {
    AddToken,
    DeleteToken,
    GetTokensList,
    GetAccessToken,
    GetRefreshToken,
    UpdateRefreshToken,
    UpdateAccessToken,
    UpdateToken,
    GetToken,
    CheckToken,
};
