"use strict";

import Mongo from '../../connectors/mongo';

const Init = () => {
    const Tokens = new Mongo.Connect().Schema({
        User_id: {
            type: String,
            unique: true,
        },
        AccessToken: String,
        RefreshToken: String,
        createAt: {
            type: Date,
            index: {
                expires: 60 * 60 * 5
            },
            default: Date.now
        }
    });

    Mongo.Connect().model('Tokens', Tokens)
};

export default {
    Init,
};
