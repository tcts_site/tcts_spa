"use strict";

import Users from './users';
import Tokens from './tokens';
import Price from './price';
import Sessions from './sessions';

const RegModels = () => {
    Users.Init();
    Tokens.Init();
    Price.Init();
    Sessions.Init();
};

export default {
    RegModels,
};
