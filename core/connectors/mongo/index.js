"use strict";

import mongoose from 'mongoose';
import settings from '../../settings';
import logs from '../../logs';

let log = logs.Get().log.bind(logs.Get());
let instanceMongo = null;

const Connect = () => {
    if (instanceMongo === null) {
        const config = settings.Get().DB;

        instanceMongo = mongoose;
        instanceMongo.set('useFindAndModify', false);
        instanceMongo.connect(config.Url + config.DbName, {useNewUrlParser: true,  useCreateIndex: true}).then(() => {
                log('debug', 'Open connection to mongodb: %s', config.Url + config.DbName);
            }, err => {
                console.error.bind(console, "Cannot connect to Mongo :", err);
            }
        );
    }

    return instanceMongo;
};

const OpenConnect = () => {
    Connect();
};

export default {
    OpenConnect,
    Connect
};
