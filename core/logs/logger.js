"use strict";

import { createLogger, format, transports } from 'winston';
const { combine, timestamp, colorize, label, printf, splat } = format;

const customFormat = printf(info => {
    return `${info.timestamp} ${info.label} ${info.level}: ${info.message}`;
});

let instanceLogger = null;

const InitLogger = () => {
    if (instanceLogger == null) {
        instanceLogger = createLogger({
            level: 'silly',
            format: combine(
                colorize({ all: true }),
                timestamp(),
                label({ label: '[app-server]' }),
                splat(),
                customFormat,
            ),
            transports: [
                new transports.Console(),
            ],
            exitOnError: false,
        });

        return instanceLogger;
    }

    return instanceLogger;
};

const Get = () => {
    if (instanceLogger == null) {
        InitLogger();
    }
    return instanceLogger;
};

export default {
    Get,
}