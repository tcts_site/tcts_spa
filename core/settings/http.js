"use strict";

export const HTTPConfig = ({ http }) => {
    return {
        Listen: http.listen,
        Domain: http.domain,
        Secret: http.secret,
        Cors: http.cors,
        Routes: http.routes,
        Static: http.static,
        Prefix: http.prefix,
        Salt: http.salt,
        Compress: http.compress,
    }
};
