"use strict";

import { DatabaseConfig } from './mongo';
import { HTTPConfig } from './http';
import { Core } from './core';

import configs from '../../app/configs';

let settings = null;

const InitSettings = () => {
    if (settings === null) {
        settings = {
            DB: DatabaseConfig(configs),
            HTTP: HTTPConfig(configs),
            CORE: Core(configs),
        };
    }

    return settings;
};

const Get = () => {
    if (settings === null) {
        settings = InitSettings();
    }

    return settings;
};

export default {
    Get,
    InitSettings,
}
