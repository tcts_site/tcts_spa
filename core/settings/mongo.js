"use strict";

export const DatabaseConfig = ({ mongo }) => {
    if (mongo !== undefined) {
        return {
            Url: mongo.url,
            Username: mongo.username,
            Password: mongo.password,
            ReplicaSe: mongo.replicaSe,
            DbName: mongo.dbName,
        }
    }

    return null;
};
