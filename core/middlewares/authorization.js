"use strict";

import Tokens from '../models/tokens';
import { JWT } from '../library/jwt';

const verifyToken = async (req, res, next) => {
    let token = req.headers['x-access-token'];

    if (!token) {
        return res.status(403).json({
            auth: false,
            error: 'No token provided.',
        });
    }

    try {
        const { login, tokenType } = await JWT.verify(token);
        const Token = await Tokens.GetToken({ login });

        if (tokenType === 'TOKEN_TYPE_ACCESS' && Token.AccessToken === token) {
            req.login = login;
            return next();
        }

        if (tokenType === 'TOKEN_TYPE_REFRESH' && Token.RefreshToken === token) {
            req.login = login;
            return next();
        }

        return res.status(403).json({
            auth: false,
            error: 'Fail to Authentication. Error 1 ->'
        });
    }
    catch (error) {
        return res.status(403).json({
            auth: false,
            error: 'Fail to Authentication. Error 1 ->'
        });
    }
};

export default {
    verifyToken: verifyToken,
}