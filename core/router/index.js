"use strict";

import { Router } from "./router_type";
import settings from "../settings";

let appRouter = null;

const GetRouter = () => {
    if (appRouter === null) {
        appRouter = new Router();
        appRouter.Middlewares = [];
        appRouter.Nodes = [];
        appRouter.parseNodes(settings.Get().HTTP.Routes);
        appRouter.compilePlugins(settings.Get().CORE.Controllers);
        appRouter.InitMiddlewares(appRouter);
        appRouter.initRouting();
    }

    return appRouter;
};

const CreateRouter = () => {
    GetRouter();
};

export default {
    CreateRouter,
};

