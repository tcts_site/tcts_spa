"use strict";

import * as http from "http";
import path from "path";
import express from "express";
import favicon from "serve-favicon";
import compress from "compression";
import cors from "cors";
import bodyParser from "body-parser";

import settings from "../settings";
import logs from "../logs";
import GetMiddleWares from "./middlewares";

export let Node = class {
    Path = '';
    Method = '';
    Controller = '';
    Middleware = [];
};

export let Router = class {
    Router = null;
    App = null;
    Nodes = [];
    NotFoundHandle = '';
    Middlewares = [];
    ControllerPlugin = null;
    InitMiddlewares = GetMiddleWares;

    GetHandleFunc = funcName => {
        return this.ControllerPlugin[funcName];
    };

    GetHandleFuncWithMiddleware = funcName => {
        return this.ControllerPlugin[funcName];
    };

    GetMiddleware = funcName => this.Middlewares[funcName];

    AddMiddleware = (name, midHandle) => {
        this.Middlewares[name] = midHandle;
    };

    parseNodes = nodes => {
        if (nodes["nodes"]) {
            nodes["nodes"].forEach(item => {
                let node = item;
                let middlewares = [];

                if (node["middlewares"]) {
                    middlewares = node["middlewares"].map(item => item);
                }

                this.AddNode(
                    item['path'],
                    item['method'],
                    item['controller'],
                    middlewares,
                );
            });
        }
    };

    AddNode = (path, method, controller, middlewares) => {
        const node = new Node();

        node.Middleware = middlewares;
        node.Path = path;
        node.Method = method;
        node.Controller = controller;

        this.Nodes.push(node);
    };

    initRouting = () => {
        const httpSettings = settings.Get().HTTP;
        this.App = this.App === null ? express() : this.App;
        this.Router = this.Router === null ? express.Router() : this.Router;

        this.App.use(bodyParser.json());
        this.App.use(bodyParser.urlencoded({extended: true}));


        if (httpSettings.Static.length > 0) {
            httpSettings.Static.forEach(staticPath => {
                if (process.env.NODE_ENV !== 'production') {
                    const webpack = require('webpack');
                    const configFactory = require('../../webpack/config');
                    const webpackDevMiddleware = require('webpack-dev-middleware');
                    const webpackHotMiddleware = require('webpack-hot-middleware');

                    const config = configFactory('development');
                    const compiler = webpack(config);
                    const devMiddleware = webpackDevMiddleware(compiler, {
                        noInfo: true,
                        silent: true,
                        stats: 'errors-only',
                        publicPath: config.output.publicPath,
                        watchOptions: {
                            ignored: /node_modules/
                        }
                    });

                    const hotMiddleware = webpackHotMiddleware(compiler, {
                        log: console.log,
                        path: '/__webpack_hmr', heartbeat: 10 * 1000
                    });

                    this.App.use([devMiddleware, hotMiddleware]);

                } else {
                    this.App.use('/', express.static(staticPath));
                }

                this.App.set('view engine', 'ejs');
                this.App.set('views', path.join(staticPath, 'views'));
                this.App.use(favicon(path.join(staticPath, 'favicon.ico')));
            });
        }

        if (httpSettings.Compress) {
            this.App.use(compress());
        }

        if (httpSettings.Cors) {
            this.App.use(cors({
                origin: "*",
                allowedHeaders: ["*"],
                methods: ["GET", "POST", "PUT", "DELETE"],
            }));
        }

        this.Nodes.forEach(node => {
            if (node.Middleware.length === 0) {
                const handleFunc = this.GetHandleFunc(node.Controller);
                this.Router[node.Method](node.Path, handleFunc);
            } else {
                const handleFunc = this.GetHandleFuncWithMiddleware(node.Controller);
                node.Middleware.forEach(middlewareName => {
                    const handleFuncMiddleware = this.GetMiddleware(middlewareName);
                    this.Router[node.Method](node.Path, handleFuncMiddleware, handleFunc);
                })
            }
        });

        this.App.use(httpSettings.Prefix, this.Router);

        http.createServer(this.App).listen(httpSettings.Listen, () => {
            logs.Get().log.bind(logs.Get())('debug', `Start listening on http://${httpSettings.Domain}:${httpSettings.Listen}. Mode: ${process.env.NODE_ENV}`);
        });
    };

    compilePlugins = controllers => {
        this.ControllerPlugin = controllers;
    }
};
