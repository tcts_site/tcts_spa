"use strict";

import bcrypt from 'bcryptjs';

export const MakeHash = password => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(8, (error, salt) => {
            if (error) return reject(error);

            bcrypt.hash(password, salt, (error, hash) => {
                if (error) return reject(error);
                return resolve(hash)
            })
        })
    })
};

export const CheckPass = (password, hash) => {
    return bcrypt.compare(password, hash);
};

