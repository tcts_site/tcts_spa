"use strict";

import jwt from 'jsonwebtoken';
import settings from "../settings";

const accessConfig = {
    expiresIn: 60 * 60, // 1 час
};

const refreshConfig = {
    expiresIn: 60 * 60 * 5, // 5 часов
};

export const JWT = {
    sign: async (payload, options) => {
        const SECRET = settings.Get().HTTP.Secret;
        return await jwt.sign(payload, SECRET, options);
    },

    verify: async (token) => {
        const SECRET = settings.Get().HTTP.Secret;
        return await jwt.verify(token, SECRET);
    }
};

export const MakeAccessToken = async userEntity => {
    const { expiresIn } = accessConfig;

    let config = {
        payload: {
            tokenType: 'TOKEN_TYPE_ACCESS',
            login: userEntity.Login,
        },

        options: {
            algorithm: 'HS512',
            subject: userEntity.id.toString(),
            expiresIn,
        }
    };

    return await JWT.sign(config.payload, config.options)
};

export const MakeRefreshToken = async userEntity => {
    const { expiresIn } = refreshConfig;

    let config = {
        payload: {
            tokenType: 'TOKEN_TYPE_REFRESH',
            login: userEntity.Login
        },

        options: {
            algorithm: 'HS512',
            subject: userEntity.id.toString(),
            expiresIn,
        }
    };

    const timestamp = new Date().getTime();
    const token = await JWT.sign(config.payload, config.options);

    return `${timestamp}::${token}`
};
