"use strict";

let Server = class {
    Modules = [];
    ApplicationType = '';

    /** Добавить модули */
    AddModule = (...modules) => {
        this.Modules = modules.map(module => module);
    };

    /** Загрузить основные модули */
    loadModules = () => this.Modules.forEach(module => {
        module();
    });

    /** Запуск сервера */
    Run = () => this.loadModules();
};

let AppServer = new Server();

/** Создать основу */
const GetAppServer = () => {
    if (AppServer === null) {
        AppServer = new Server();
    }

    return AppServer;
};

export default {
    GetAppServer,
};
