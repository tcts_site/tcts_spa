FROM node:10

# Создаем рабочую директорию
WORKDIR /app

# Устанавливаем среду разработки
ENV NODE_ENV=production

# Копируем в неё файлы package.json и package-lock.json
COPY ./package*.json ./

# Выполняем установку пакетов npm
RUN npm install --only=production

# Копируем остальные файлы приложения
COPY ./server.min.js ./
COPY ./app/static ./app/static

# Открываем наружу порт 8081
EXPOSE 8081

# Указываем команду, выполняемую при запуске контейнера
CMD [ "npm", "run", "start" ]