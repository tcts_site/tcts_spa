process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';
process.env.SSR = 'true';

const fs = require('fs');
const chalk = require('chalk');

process.on('unhandledRejection', err => {
    throw err;
});

console.log(chalk.cyan('Starting the development server...\n'));

const babelrc = JSON.parse(fs.readFileSync('./.babelrc'));
require("@babel/register")(babelrc);
require( "../app/index.js" );